// C++ includes. Should remove.
#include <iostream>

// C includes.
#include <stdio.h>

#include <GL/glew.h>
#include <GL/gl.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include "SDL2/SDL_ttf.h"
#include "SDL2_gfxPrimitives.h"

#include "./dbg.h"
#include "./macros.h"
#include "./math.h"
#include "./vector.h"
#include "./containers.h"
#include "./hotloader.h"

#include "./renderer.h"

////////////////////////////////////////
///    Types
////////////////////////////////////////

#define hex(c) HEX_COLOR(((int)c.x << 24) | ((int)c.y << 16) |((int)c.z << 8) |((int)c.w << 0))

////////////////////////////////////////
///    Game
////////////////////////////////////////
struct App {
    SDL_Window* window { nullptr };
    SDL_Renderer* sdl_renderer { nullptr };
    SDL_GLContext gl_context;

    Renderer renderer;

    bool quit { false };

    float dt;

    struct {
        Vec2i pos { };
        bool pressed { false };
        bool released { false };
    } mouse_state;

    struct {
        Vec2i tile_size  = Vec2i( 50, 50 );

        Rgba tile_empty_color  = Rgba(0x565857FF);
        Rgba tile_wall_color   = Rgba(0x8A8D91FF);
        Rgba tile_player_color = Rgba(0xF5D3C8FF);
        Rgba tile_target_color = Rgba(0xE98A15FF);
        Rgba tile_objective_color = Rgba(0X734B5EFF);
        Rgba tile_door_open_color = Rgba(0X378A41FF);
        Rgba tile_door_closed_color = Rgba(0X732420FF);

        Rgba bg_color { 0x27, 0x27, 0x27, 0xFF };
    } visuals;
};

struct Grid_Tile
{
    enum class Kind
    {
        PLAYER = 0,
        WALL,
        OBJECTIVE,
        TARGET,
        DOOR,

        COUNT
    };

    Kind kind { Kind::COUNT };
    Vec2i grid_coord { 0, 0 };

    bool is_door_open { true };

    Grid_Tile() { }
    Grid_Tile(Kind t) : kind(t) {}
    Grid_Tile(int x, int y, Kind k) : kind(k), grid_coord(Vec2i(x, y)) {}
    Grid_Tile(Vec2i pos) : grid_coord(pos) {}

    template<typename OStream>
    friend OStream &operator<<(OStream &os, const Grid_Tile &c)
    {
        return os << "[ Tile: " << c.kind << " at " << c.grid_coord.x << ", " << c.grid_coord.y << " ]";
    }
};

const char* human(Grid_Tile::Kind kind) {
    switch(kind) {
    case Grid_Tile::Kind::PLAYER:    return "player";
    case Grid_Tile::Kind::WALL:      return "wall";
    case Grid_Tile::Kind::OBJECTIVE: return "objective";
    case Grid_Tile::Kind::TARGET:    return "target";
    case Grid_Tile::Kind::DOOR:    return "door";

    case Grid_Tile::Kind::COUNT:
    default:
        UNREACHABLE();
    }


    UNREACHABLE();
}

Grid_Tile::Kind from_human(String_View human)
{
    if (human == "player"_sv)    return Grid_Tile::Kind::PLAYER;
    if (human == "wall"_sv)      return Grid_Tile::Kind::WALL;
    if (human == "objective"_sv) return Grid_Tile::Kind::OBJECTIVE;
    if (human == "target"_sv)    return Grid_Tile::Kind::TARGET;
    if (human == "door"_sv)      return Grid_Tile::Kind::DOOR;

    UNREACHABLE();
}

enum class Direction
{
    NORTH = 0,
    SOUTH,
    EAST,
    WEST
};

using Grid_Coord = Vec2i;

struct Level; // @Forward-decl.

struct Player
{
    struct Move_Info
    {
        bool can_actually_move { false };
        bool target_is_empty   { true  };

        Grid_Tile *target_tile { nullptr };
    };

    Level *level { nullptr };

    Grid_Coord grid_coord { };
    Grid_Coord previous_grid_coord { };

    Direction facing { Direction::EAST };

    struct {
        bool is_jumping       { false };
        float jump_time       { 1.0f };
        float jump_t          { 0.0f };
        Vec2f jump_target_pos { 0.0f, 0.0f };
    } animation;
};

///
///    Globals
///

// Color palette
Rgba palette_bg    { 0x12, 0x12, 0x24, 0xFF };
Rgba palette_fg    { 0xBC, 0xBC, 0xBC, 0xFF };
Rgba palette_error { 0xBC, 0x24, 0x24, 0xFF };
Rgba palette_warn  { 0xBC, 0x24, 0xBC, 0xFF };

const char*   APP_NAME = "Sokoban";
int           WINDOW_WIDTH  = 1920/2;
int           WINDOW_HEIGHT = 1080/2;
int           WINDOW_PADDING = 50;
constexpr u32 RENDER_FLAGS  = SDL_RENDERER_ACCELERATED;
constexpr u32 WINDOW_FLAGS  = SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN;
App           app { };
TTF_Font      *debug_font { nullptr };

enum class Game_State
{
    PLAYING = 0,
    PAUSED,
    DEBUG,
};

struct Level
{
    String_View name { };

    Vec2f         screen_pos  { };

    // @Todo: This should probably be a HashSet<Vec2i, Grid_Tile_Info>
    Bucket_Hash_Map<Grid_Coord, Grid_Tile> tiles { };
    Vec2i         dimensions { };

    Player player { };
    File save_data { };

    Level() = default;
};

struct Game
{
    Level   current_level   { };
    Game_State state { };
};

Vec2i grid_coord_to_screen_space(Grid_Coord grid_coord, const Vec2i &grid_pos) {
    return grid_coord*app.visuals.tile_size +
           grid_pos + WINDOW_PADDING*0.5f;
}

Vec2i coordinate_in_direction(Vec2i pos, const Level& grid, Direction dir)
{
    switch(dir) {
    case Direction::NORTH: return { pos.x, max((pos.y - 1), 0) };
    case Direction::SOUTH: return { pos.x, min((pos.y + 1), grid.dimensions.y-1) };
    case Direction::EAST:  return {min(pos.x + 1, grid.dimensions.x-1), pos.y};
    case Direction::WEST:  return {max(pos.x - 1, 0), pos.y};

    default: UNREACHABLE();
    }
}

Maybe<Grid_Tile*> tile_in_front(const Player & player) {
    if (!player.level) return nothing<Grid_Tile*>();

    auto target = coordinate_in_direction(player.grid_coord, *player.level, player.facing);

    auto level = player.level;
    if (!level)  return nothing<Grid_Tile*>();

    auto tiles_at_target = level->tiles.get_bucket(target);

    for (size_t i = 0; i < tiles_at_target.values.count; ++i) {
        // @Incomplete: could priritise picking an objective tile here.
        return just(&tiles_at_target.values[i]);
    }

    return nothing<Grid_Tile*>();
}

Vec2i dir_vec_old(Direction dir) {
    switch(dir) {
        case Direction::NORTH: return Vec2i( 0, -1);
        case Direction::SOUTH: return Vec2i( 0, 1);
        case Direction::EAST:  return Vec2i( 1, 0);
        case Direction::WEST:  return Vec2i(-1, 0);
        default: UNREACHABLE();
    }
}

glm::vec2 dir_vec(Direction dir) {
    switch(dir) {
        case Direction::NORTH: return glm::vec2( 0, -1);
        case Direction::SOUTH: return glm::vec2( 0, 1);
        case Direction::EAST:  return glm::vec2( 1, 0);
        case Direction::WEST:  return glm::vec2(-1, 0);
        default: UNREACHABLE();
    }
}

struct Slide_Tile_Result
{
    bool did_actually_slide { false };
    bool on_open_door       { false };
    bool was_objective_hitting_target { false };
};

Slide_Tile_Result slide_tile(Level* level, Grid_Tile &tile, Direction dir)
{
    Slide_Tile_Result result { };
    auto target_coord = coordinate_in_direction(tile.grid_coord, *level, dir);

    auto tiles_at_target = level->tiles.get_bucket(target_coord);

    for (size_t i = 0; i < tiles_at_target.values.count; ++i) {
        auto &target_tile = tiles_at_target.values[i];
        if (tile.kind == Grid_Tile::Kind::DOOR) {
            if (tile.is_door_open) {
                result.on_open_door = true;
                break;
            }
        }

        if (target_tile.kind == Grid_Tile::Kind::TARGET && tile.kind == Grid_Tile::Kind::OBJECTIVE)
        {
            result.was_objective_hitting_target = true;
            break;
        }

        // Hit something the tile can't move through.
        result.did_actually_slide = false;
        return result;
    }

    result.did_actually_slide = true;

    tile.grid_coord = target_coord;

    return result;
}

Player::Move_Info can_move(Player const& player)
{
    Player::Move_Info move_info {};
    move_info.can_actually_move = false;

    if (!player.level)     return move_info;

    auto &level = *player.level;

    auto target_coord = coordinate_in_direction(player.grid_coord, *player.level, player.facing);

    if (target_coord.x < 0 || target_coord.y < 0 || target_coord.x >= level.dimensions.x || target_coord.y >= level.dimensions.y) {
        move_info.can_actually_move = false;
        return move_info;
    }

    move_info.can_actually_move = true;
    move_info.target_is_empty = true;

    auto tiles_at_target = level.tiles.get_bucket(target_coord);
    if (tiles_at_target.values.count == 0)
        return move_info;

    for (size_t i=0; i < tiles_at_target.values.count; i++) {
        if (tiles_at_target.values[i].kind == Grid_Tile::Kind::OBJECTIVE) {
            move_info.can_actually_move = true;
            return move_info;
        }
    }

    // @Cleanup: which tile should we return here?
    move_info.target_tile = &tiles_at_target.values[0];
    move_info.target_is_empty   = false;
    move_info.can_actually_move = false;

    return move_info;
}

bool move_player(Player &player, Direction dir)
{
    player.facing = dir;

    auto move_info = can_move(player);

    if (!move_info.can_actually_move)  return false;

    auto &grid = player.level;
    auto &tile_in_front = *move_info.target_tile;

    if (!move_info.target_is_empty) {
        if (tile_in_front.kind == Grid_Tile::Kind::OBJECTIVE) {  // Push the box in front of us
            auto slide_result = slide_tile(grid, tile_in_front, dir);
            if (!slide_result.did_actually_slide) {
                return false;
            }

            if (slide_result.was_objective_hitting_target) {
            }
        }
    }

    // Move into the gap in front of the player.
    player.previous_grid_coord = player.grid_coord;

    auto &pos  = player.grid_coord;

    switch(dir) { 
    case Direction::NORTH: { pos.y = max(pos.y-1, 0); } break;
    case Direction::SOUTH: { pos.y = min(pos.y+1, grid->dimensions.y); } break;
    case Direction::EAST:  { pos.x = max(pos.x+1, 0); } break;
    case Direction::WEST:  { pos.x = min(pos.x-1, grid->dimensions.x); } break;

    default: ASSERT(0 && "Unreachable.");
    }

    player.facing = dir;

    player.animation.is_jumping = true;
    auto target_veci = grid_coord_to_screen_space(pos, floor(player.level->screen_pos));
    player.animation.jump_target_pos.x = (float)target_veci.x;
    player.animation.jump_target_pos.y = (float)target_veci.y;

    return true;
}

auto dir(SDL_Scancode key)
{
    if (key == SDL_SCANCODE_W || key == SDL_SCANCODE_UP)     return Direction::NORTH;
    if (key == SDL_SCANCODE_S || key == SDL_SCANCODE_DOWN)   return Direction::SOUTH;
    if (key == SDL_SCANCODE_A || key == SDL_SCANCODE_LEFT)   return Direction::WEST;
    if (key == SDL_SCANCODE_D || key == SDL_SCANCODE_RIGHT)  return Direction::EAST;

    UNREACHABLE();
}

bool change_state(Game &game, Game_State state) {
    // @Todo: Game_State transition table
    switch(game.state) {
    case Game_State::PLAYING: {
        if (state == Game_State::DEBUG) {
            game.state = state;
            return true;
        }
    } break;
    case Game_State::PAUSED: {

    } break;
    case Game_State::DEBUG: {
        if (state == Game_State::PLAYING) {
            game.state = state;
            return true;
        }
    } break;

    default: UNREACHABLE();
    }

    return false;
};


////////////////////////////////////////
///    SDL layer
////////////////////////////////////////
#define lib_error(lib, message, ...)                        \
    do {                                                    \
        fprintf(stderr, "[" lib " ERROR]: ");               \
        fprintf(stderr, "%s\n\t", message, ##__VA_ARGS__);  \
        fprintf(stderr, "\n");                              \
        fflush(stderr);                                     \
        exit(1);                                         \
    } while(0);

#define SDL_ERROR(message, ...) lib_error("SDL", message, ##__VA_ARGS__)
#define TTF_ERROR(message, ...) lib_error("TTF", message, ##__VA_ARGS__)

int sc(int code)
{
    if (code < 0) {
        SDL_ERROR(SDL_GetError());
    }

    return code;
}

void* sc(void* code)
{
    if (!code) {
        SDL_ERROR(SDL_GetError());
    }

    return code;
}

int tc(int code)
{
    if (code < 0) {
        TTF_ERROR(TTF_GetError());
    }

    return code;
}

void* tc(void* code)
{
    if (!code) {
        TTF_ERROR(TTF_GetError());
    }

    return code;
}

void init_renderer(void)
{
    ///
    /// Init SDL
    ///

    sc(SDL_Init(SDL_INIT_EVERYTHING));
    printf("[INFO] Initialised SDL Version %d.%d\n", SDL_MAJOR_VERSION, SDL_MINOR_VERSION);

    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    app.window   = (SDL_Window*) sc(SDL_CreateWindow(APP_NAME, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, WINDOW_WIDTH, WINDOW_HEIGHT, WINDOW_FLAGS));
    SDL_SetHint(SDL_HINT_RENDER_SCALE_QUALITY, "linear");

    app.gl_context = (SDL_GLContext) sc(SDL_GL_CreateContext(app.window));
    SDL_GL_SetSwapInterval(1);

    app.sdl_renderer = (SDL_Renderer*) sc(SDL_CreateRenderer(app.window, -1, RENDER_FLAGS));

    if (!init_gl(app.renderer, WINDOW_WIDTH, WINDOW_HEIGHT)) {
        spdlog::error("There were errors initializing the OpenGL context. Hopefully there's some explanation why above ^.");
    }

    ///
    /// Init font rendering
    ///

    tc(TTF_Init());
    printf("[INFO] Initialised TTF Version %d.%d\n", TTF_MAJOR_VERSION, TTF_MINOR_VERSION);

    debug_font = (TTF_Font*) tc(TTF_OpenFont("./src/debug.ttf", 24));
}

Maybe<Vec2i> tile_coord_at_mouse_pos(Vec2i mouse_pos, const Level& level)
{
    Vec2i result = (mouse_pos - WINDOW_PADDING*0.5f) / app.visuals.tile_size;

    if (result.x > level.dimensions.x || result.x < 0 || result.y > level.dimensions.y || result.y < 0)
        return nothing<Vec2i>();

    return just(result);
}

Maybe<Grid_Tile*> tile_at_mouse_pos(Level & grid, Vec2i mouse_pos)
{
    // auto pos   = Vec2i(x, y)*gfx.tile_size + gfx.tile_margin*0.5f + WINDOW_PADDING*0.5f;

    auto tile_coord = tile_coord_at_mouse_pos(mouse_pos, grid);
    if (!tile_coord.has_value)   return nothing<Grid_Tile*>();

    auto tiles_at_target = grid.tiles.get_bucket(tile_coord.value);
    if (tiles_at_target.values.count > 0) {
        // @Incomplete: how to choose which tile to return here.
        return just<Grid_Tile*>(&tiles_at_target.values[0]);
    }

    return nothing<Grid_Tile*>();
}

struct Migui {
    using Id = ssize_t;

    struct Layout {
        enum class Kind {
            HORIZONTAL = 0,
            VERTICAL,
        };

        Kind kind { Migui::Layout::Kind::HORIZONTAL };

        Vec2i screen_pos { 0, 0 };
        Vec2i screen_size { 0, 0 };
        int   padding { 10 };

        Vec2i cursor() const
        {
            switch(this->kind) {
            case Layout::Kind::HORIZONTAL:
                return screen_pos + (screen_size+Vec2i(padding)) * Vec2i(1, 0);
            case Layout::Kind::VERTICAL:
                return screen_pos + (screen_size+Vec2i(padding)) * Vec2i(0, 1);

            default: UNREACHABLE();
            }
        }

        void expand_by_widget_size(Vec2i size) {
            switch(kind) {
            case Kind::HORIZONTAL: {
                screen_size.x = screen_size.x + size.x + padding;
                screen_size.y = max(size.y, screen_size.y);

                // layout.screen_size = layout.cursor + Vec2i(1, 0)*layout.padding;
            } break;

            case Migui::Layout::Kind::VERTICAL: {
                screen_size.x = max(size.x, screen_size.x);
                screen_size.y = screen_size.y + size.y + padding;

                // layout.screen_size = layout.cursor + Vec2i(0, 1)*layout.padding;
            } break;

            default:  UNREACHABLE();
            }
        }
    };

    Vec2i screen_size { 0, 0 };

    Maybe<Id> hot { };
    Maybe<Id> active { };

    Stack<Layout> layout_stack;
    int widget_count { -1 };

    struct {
        Hsla bg = Hsla(100.0f/360.0f, 0.18f, 0.13f);
        Hsla fg = Hsla(32.0f/360.0f, 1.0f, 0.8f);
        Hsla button_active = Hsla(198.0f/360.0f, 0.82f, 0.23f);
        Hsla button_bg = Hsla(131.0f/360.0f, 0.11, 0.49);
    } color_palette;
};

template <> // @Todo: unecessary once I figure out the template bullshit
Maybe<Migui::Id> nothing() {
    Maybe<Migui::Id> m;

    m.has_value = false;
    m.value = -1;

    return m;
}

struct Tile_Tool {
    enum class Type {
        CLEAR = 0,
        PLACE,
        COUNT
    };

    Tile_Tool::Type type;
    Grid_Tile::Kind tile_type;
};

String_View human_name(Tile_Tool::Type type)
{
    switch(type) {
    case Tile_Tool::Type::CLEAR:
        return "Clear"_sv;
    case Tile_Tool::Type::PLACE:
        return "Place"_sv;
        break;

    case Tile_Tool::Type::COUNT:
    default:
        UNREACHABLE();
    }

    UNREACHABLE();
}

struct Debug_Panel {
    Migui ui { };

    bool   is_open { false };
    Vec2i  screen_pos { 0, 0 };

    // @Todo: bring back sliding open animation
    // Vec2i  target_size { };
    // float     openness { 0.0f };
    // float     open_speed { 0.00001f };

    Tile_Tool active_tool { Tile_Tool::Type::CLEAR };
    Grid_Tile::Kind tile_being_placed { Grid_Tile::Kind::OBJECTIVE };

    Maybe<Grid_Tile*> door_being_connected { };
} debug_panel;

void save_level(Level &level)
{
    clear(level.save_data);

    append(level.save_data, "level\n");

    append(level.save_data, "name ");
    append(level.save_data, level.name);
    append(level.save_data, "\n");

    append(level.save_data, "size ");
    append(level.save_data, level.dimensions.x);
    append(level.save_data, " ");
    append(level.save_data, level.dimensions.y);
    append(level.save_data, "\n");
    append(level.save_data, "\n");

    append(level.save_data, "player_coord ");
    append(level.save_data, level.player.grid_coord.x);
    append(level.save_data, " ");
    append(level.save_data, level.player.grid_coord.y);
    append(level.save_data, "\n");

    append(level.save_data, "player_facing ");
    append(level.save_data, (u64)level.player.facing);
    append(level.save_data, "\n");
    append(level.save_data, "\n");

    append(level.save_data, "tiles ");
    append(level.save_data, level.tiles.count);
    append(level.save_data, "\n");

    for (size_t i = 0; i < level.tiles.buckets.count; ++i) {
        auto bucket = level.tiles.buckets[i];
        for (size_t j = 0; j < bucket.values.count; ++j) {
            auto tile = bucket.values[j];
            append(level.save_data, "- ");
            append(level.save_data, "coord ");
            append(level.save_data, tile.grid_coord.x);
            append(level.save_data, " ");
            append(level.save_data, tile.grid_coord.y);
            append(level.save_data, " kind ");
            append(level.save_data, human(tile.kind));
            append(level.save_data, "\n");
        }
    }
}

// @Robustness: this thing needs safety pass.
// @Robustness: this thing needs safety pass.
// @Robustness: this thing needs safety pass.
// @Robustness: this thing needs safety pass.
// @Robustness: this thing needs safety pass.
// @Robustness: this thing needs safety pass.
// @Robustness: this thing needs safety pass.
bool load_level(Level &level, const char* filepath)
{
    level.player = {};
    level.tiles = Bucket_Hash_Map<Grid_Coord, Grid_Tile>();
    level.player.level = &level;

    level.save_data = open(filepath, File::Mode::READING);
    auto file_sv = String_View(level.save_data.contents, level.save_data.contents_size);

    String_View line = eat_line(file_sv);
    if (line != "level"_sv) {
        auto line_c_str = to_c_str(line);
        printf("ERROR: load_file expected to find `level` at top of level file, but found `%s`\n", line_c_str);
        return false;
    }

    line = eat_line(file_sv);
    while (line.count > 0)
    {
        auto directive = eat_word(line);
        if (directive.count == 0)  break;

        if (directive == "name"_sv) {
            level.name = eat_word(line);
        }
        else if (directive == "size"_sv) {
            auto width_sv = eat_word(line);
            auto height_sv = eat_word(line);

            auto width = to_u64(width_sv);
            auto height = to_u64(height_sv);

            level.dimensions = Vec2i(width, height);
        }
        else if (directive == "player_coord"_sv) {
            auto player_x_sv = eat_word(line);
            auto player_y_sv = eat_word(line);

            auto player_x = to_u64(player_x_sv);
            auto player_y = to_u64(player_y_sv);

            level.player.grid_coord = Vec2i(player_x, player_y);
        }
        else if (directive == "player_facing"_sv) {
            auto player_facing = (Direction)to_u64(eat_word(line));

            level.player.facing = player_facing;
        }
        else if (directive == "tiles"_sv) {
            auto tile_count = to_u64(eat_word(line));

            auto tile_line = eat_line(file_sv);
            for (size_t i = 0; i < tile_count; ++i) {
                Grid_Tile tile = {};

                eat_word(tile_line); // Chomp the `- ` at the start of the line.

                auto word = eat_word(tile_line);

                while(word.count > 0) {
                    if (word == "coord"_sv) {
                        tile.grid_coord.x = to_u64(eat_word(tile_line));
                        tile.grid_coord.y = to_u64(eat_word(tile_line));
                    }
                    else if (word == "kind"_sv) {
                        tile.kind = from_human(eat_word(tile_line));
                    }

                    if (tile_line.data[0] == '-')  break; // Finished this tile cos we hit the next tile's '-'

                    word = eat_word(tile_line);
                }

                // spdlog::info("Putting {} -> {}", tile.grid_coord, tile);
                level.tiles.put(tile.grid_coord, tile);

                tile_line = eat_line(file_sv);
            }
        }
        else {
            printf("ERROR: load_file found unrecognized directive `%s`\n", to_c_str(directive));
            return false;
        }

        line = eat_line(file_sv);
        if (line.count == 0)  break;
    }

    return true;
}

void handle_input(Game &game)
{
    auto& player = game.current_level.player;

    SDL_Event e;
    while (SDL_PollEvent(&e)) {
        if (SDL_QUIT == e.type) {
            app.quit = true;
            break;
        }
        else if (SDL_KEYDOWN == e.type) {
            if (SDL_SCANCODE_F2 == e.key.keysym.scancode) {
                save_level(game.current_level);
            }
            else if (SDL_SCANCODE_F3 == e.key.keysym.scancode) {
                load_level(game.current_level, "sandbox.level_data");
            }
            else if (SDL_SCANCODE_Q == e.key.keysym.scancode) {
                app.quit = true;
                break;
            }
            else if (SDL_SCANCODE_R == e.key.keysym.scancode) {
                auto camera_rot_speed = 4.0f;

                rotate(app.renderer.camera, app.renderer.camera.z_rot + camera_rot_speed);
            }
            else if ((SDL_SCANCODE_W == e.key.keysym.scancode) ||
                    (SDL_SCANCODE_S == e.key.keysym.scancode) ||
                    (SDL_SCANCODE_D == e.key.keysym.scancode) ||
                    (SDL_SCANCODE_A == e.key.keysym.scancode)) {
                if (game.state == Game_State::PLAYING) {
                    auto dir_pressed = dir(e.key.keysym.scancode);

                    move_player(player, dir_pressed);

                    auto camera_speed = 8.0f;
                    move(app.renderer.camera, app.renderer.camera.pos +
                                                  camera_speed * glm::vec3(dir_vec(dir_pressed), 0)/app.dt);
                }
            }
            else if (SDL_SCANCODE_F1 == e.key.keysym.scancode) {
                if (debug_panel.is_open)  {
                    change_state(game, Game_State::PLAYING);

                    debug_panel.is_open = false;
                }
                else {
                    change_state(game, Game_State::DEBUG);

                    debug_panel.is_open = true;
                }
            }
        }
        else if (e.type == SDL_MOUSEMOTION ||
            e.type == SDL_MOUSEBUTTONDOWN ||
            e.type == SDL_MOUSEBUTTONUP) {
            SDL_GetMouseState(&app.mouse_state.pos.x, &app.mouse_state.pos.y);

            auto tile = tile_at_mouse_pos(game.current_level, app.mouse_state.pos); // @Copypasta

            if (e.type == SDL_MOUSEBUTTONDOWN) {
                app.mouse_state.pressed = true;
                app.mouse_state.released = false;

                if (tile.has_value) {
                    if (debug_panel.active_tool.type == Tile_Tool::Type::PLACE) {
                        tile.value->kind = debug_panel.tile_being_placed;
                    }
                    else if (debug_panel.active_tool.type == Tile_Tool::Type::CLEAR) {
                        // Find the index of the tile. I'll probably need to move this out
                        TODO("Clearing tiles.");
                        // auto &grid = game.current_level;
                        // for (size_t i=0; i < grid.tiles.count-1; i++) {
                        //     if (grid.tiles[i].grid_coord == tile.value->grid_coord) {
                        //         grid.tiles.remove(i);
                        //         break;
                        //     }
                        // }
                    }
                }
                else {  // No existing tile at mouse location
                    if (debug_panel.active_tool.type == Tile_Tool::Type::PLACE) {
                        auto maybe_tile_coord = tile_coord_at_mouse_pos(app.mouse_state.pos, game.current_level);
                        if (!maybe_tile_coord.has_value) continue;

                        auto tile_coord = maybe_tile_coord.value;
                        game.current_level.tiles.put(tile_coord, Grid_Tile(tile_coord.x, tile_coord.y, debug_panel.tile_being_placed));
                    }
                }
            }
            else if (e.type == SDL_MOUSEBUTTONUP) {
                app.mouse_state.pressed = false;
                app.mouse_state.released = true;
            }
            else if (e.type == SDL_MOUSEMOTION) {
                if (!app.mouse_state.pressed) continue;

                if (tile.has_value) {
                    if (debug_panel.active_tool.type == Tile_Tool::Type::PLACE) {
                        tile.value->kind = debug_panel.tile_being_placed;
                    }
                    else if (debug_panel.active_tool.type == Tile_Tool::Type::CLEAR) {
                        // Find the index of the tile. I'll probably need to move this out
                        TODO("Clearing tiles on drag.");
                        // auto &grid = game.current_level;
                        // for (size_t i=0; i < grid.tiles.count-1; i++) {
                        //     if (grid.tiles[i].grid_coord == tile.value->grid_coord) {
                        //         grid.tiles.remove(i);
                        //         break;
                        //     }
                        // }
                    }
                }
                else {  // Didn't drag over a tile.
                    if (debug_panel.active_tool.type == Tile_Tool::Type::PLACE) {
                        auto maybe_tile_coord = tile_coord_at_mouse_pos(app.mouse_state.pos, game.current_level);
                        if (!maybe_tile_coord.has_value) continue;

                        auto tile_coord = maybe_tile_coord.value;
                        game.current_level.tiles.put(tile_coord, Grid_Tile(tile_coord.x, tile_coord.y, debug_panel.tile_being_placed));
                    }
                }
            }
        }
    }
}

////////////////////////////////////////
///    Rendering
////////////////////////////////////////
void fill_quad(SDL_Renderer *r, Vec2i pos, Vec2i size, Rgba color)
{
    SDL_Rect rect = {pos.xs[0], pos.xs[1], size.xs[0], size.xs[1]};
    SDL_SetRenderDrawColor(r, (Uint8)(color.xs[0]*255.0f), (Uint8)(color.xs[1]*255.0f), (Uint8)(color.xs[2]*255.0f), (Uint8)(color.xs[3]*255.0f));
    SDL_RenderFillRect(r, &rect);
}

void draw_quad(SDL_Renderer *r, Vec2i pos, Vec2i size, Rgba color)
{
    rectangleColor(r,
                   pos.x,            pos.y,
                   pos.x + size.x, pos.y + size.y,
                   color.u32());
}

void draw_line(SDL_Renderer *r, Vec2i start, Vec2i end, Rgba color, int thicc = 1)
{
    thickLineColor(r,
                   start.x,           start.y,
                   start.x + end.x, start.y + end.y,
                   thicc, color.u32());
    // SDL_SetRenderDrawColor(r, color.x()s[0], color.x()s[1], color.x()s[2], color.x()s[3]);
    // SDL_RenderDrawLine(r, start.x()s[0], start.x()s[1], end.x()s[0], end.x()s[1]);
}

void draw_point(SDL_Renderer *r, Vec2i pos, Rgba color)
{
    SDL_SetRenderDrawColor(r, color.xs[0], color.xs[1], color.xs[2], color.xs[3]);
    SDL_RenderDrawPoint(r, pos.xs[0], pos.xs[1]);
}

void update_debug_gui() {
    if (!debug_panel.is_open)  return;
}

void draw_text(String_View str,
               Vec2i pos,
               Rgba color_ = Rgba(0xFFFFFFFF))
{
    SDL_Color color = { (Uint8)(color_.r()*255.0f), (Uint8)(color_.g()*255.0f), (Uint8)(color_.b()*255.0f) };

    SDL_Surface* surf = TTF_RenderText_Solid(debug_font, str.data, color);
    defer(SDL_FreeSurface(surf));

    SDL_Texture* text = SDL_CreateTextureFromSurface(app.sdl_renderer, surf);
    defer(SDL_DestroyTexture(text));

    int text_width, text_height;
    SDL_QueryTexture(text, NULL, NULL, &text_width, &text_height);

    SDL_Rect dst = {pos.x, pos.y, text_width, text_height};

    SDL_RenderCopy(app.sdl_renderer, text, NULL, &dst);
}

void migui_begin(Migui &ui, Vec2i pos, float pad)
{
    ui.widget_count = -1;

    Migui::Layout new_layout { };
    new_layout.screen_pos = pos;
    new_layout.kind = Migui::Layout::Kind::HORIZONTAL;
    new_layout.padding = pad;
    new_layout.screen_size = { 0 };

    ui.layout_stack.push(new_layout);
}

void migui_end(Migui &ui)
{
    ASSERT(ui.layout_stack.count > 0);

    ui.layout_stack.pop();
}

void migui_begin_layout(Migui &ui, Migui::Layout::Kind kind, float pad)
{
    ASSERT(ui.layout_stack.count > 0);

    Migui::Layout new_layout { };

    new_layout.screen_pos = ui.layout_stack.peek().cursor();
    new_layout.screen_size = { 0 };
    new_layout.kind = kind;
    new_layout.padding = pad;
    // new_layout.expand_by_widget_size(Vec2i(0));

    ui.layout_stack.push(new_layout);
}

void migui_end_layout(Migui &ui)
{
    ASSERT(ui.layout_stack.count > 1);

    auto child = ui.layout_stack.pop();

    ASSERT(ui.layout_stack.count > 0);

    auto &parent = ui.layout_stack.peek();
    parent.expand_by_widget_size(child->screen_size);
}

bool rect_contains(Vec2i start, Vec2i size, Vec2i pos) {
    return (pos.x > start.x             &&
            pos.x < start.x + size.y) &&
        (pos.y > start.y             &&
         pos.y < start.y + size.y);
}

bool migui_button(Migui &ui,
                  String_View label,
                  Hsla color)
{
    ASSERT(ui.layout_stack.count > 0);

    static const auto text_padding = Vec2i(4); // @Todo: hoist this into Layout or something.

    Vec2i size;
    TTF_SizeText(debug_font, to_c_str(label), &size.x, &size.y);
    size = size + text_padding*2;

    auto &my_layout = ui.layout_stack.peek();

    auto const &pos   = my_layout.cursor();
    auto &mouse = app.mouse_state;

    bool mouse_inside_me = rect_contains(pos, size, mouse.pos);

    // @Todo: better ui ID system
    auto id_num = (Migui::Id)++ui.widget_count;
    auto id = just(id_num);

    // printf("My id is: %ld, Mouse: %d, %d\tMouse inside me? %d\tHot: %ld\tActive: %ld\n", id.value, mouse.pos.x(), mouse.pos.y(), mouse_inside_me, ui.hot.value, ui.active.value);

    bool did_fire_this_frame = false;

    if (ui.active.has_value && ui.active == id) {
        if (mouse.released) {
            if (mouse_inside_me) {
                did_fire_this_frame = true;
            }

            ui.active = nothing<Migui::Id>();
        }

        color.s() = clamp(0.0f, color.s() + 0.5f, 1.0f);
    }
    else if (ui.hot.has_value && ui.hot == id) {
        if (mouse_inside_me) {
            if (mouse.pressed)  ui.active = id;

            color.l() = clamp(0.0f, color.l() + 0.1f, 1.0f);
        }
        else {
            ui.hot = nothing<Migui::Id>();
        }
    }
    else {
        if (mouse_inside_me) {
            ui.hot = id;
        }
    }

    fill_quad(app.sdl_renderer, pos, size, rgba(color));

    draw_text(label, pos + text_padding, rgba(0));

    my_layout.expand_by_widget_size(size);

    return did_fire_this_frame;
}

void draw_game(Game & game)
{
    auto &level = game.current_level;
    auto const& gfx = app.visuals;

    auto grid_size = level.dimensions*gfx.tile_size;
    auto grid_color = gfx.tile_empty_color;
    auto grid_pos = level.screen_pos;

    fill_quad(app.sdl_renderer, grid_pos, grid_size+gfx.tile_size*0.5f, grid_color);

    // Draw the tiles
    for (size_t i = 0; i < level.tiles.buckets.count; i++) {
        auto &bucket = level.tiles.buckets[i];

        for (size_t j = 0; j < bucket.values.count; j++) {
            auto &tile = bucket.values[j];
            auto pos  = grid_coord_to_screen_space(tile.grid_coord, grid_pos);//*gfx.tile_size + gfx.tile_margin*0.5f + grid_pos; //WINDOW_PADDING*0.5f;

            auto color = gfx.tile_empty_color;
            switch (tile.kind) {
            case Grid_Tile::Kind::WALL:   {
                color = app.visuals.tile_wall_color;
            } break;
            case Grid_Tile::Kind::PLAYER: {
                color = app.visuals.tile_player_color;
            } break;
            case Grid_Tile::Kind::OBJECTIVE: {
                color = app.visuals.tile_objective_color;
            } break;
            case Grid_Tile::Kind::TARGET: {
                color = app.visuals.tile_target_color;
            } break;

            case Grid_Tile::Kind::DOOR: {
                if (tile.is_door_open)
                    color = app.visuals.tile_door_open_color;
                else
                    color = app.visuals.tile_door_closed_color;
            } break;

            case Grid_Tile::Kind::COUNT:
            default: ASSERT(0 && "Unreachable."); break;
            }

            fill_quad(app.sdl_renderer, pos, gfx.tile_size, color);
        }
    }

    // Draw the player.
    auto player = game.current_level.player;

    auto player_pos  = grid_coord_to_screen_space(player.grid_coord, grid_pos);
    if (player.animation.is_jumping)
    {
        auto previous_pos  = grid_coord_to_screen_space(player.previous_grid_coord, grid_pos);
        auto lerped_pos = lerp(previous_pos, player_pos, player.animation.jump_t);

        fill_quad(app.sdl_renderer, lerped_pos, gfx.tile_size, gfx.tile_player_color);

        // @Temp: Draw the line to show which way the player is facing...
        auto line_end = Vec2i(0, 0);
        switch(player.facing) {
            case Direction::NORTH:   line_end = Vec2i( 0, -1);  break;
            case Direction::SOUTH:   line_end = Vec2i( 0,  1); break;
            case Direction::EAST:    line_end = Vec2i( 1,  0);  break;
            case Direction::WEST:    line_end = Vec2i(-1,  0); break;

            default: UNREACHABLE();
        }

        auto line_start = lerped_pos + (gfx.tile_size*0.5f);

        draw_line(app.sdl_renderer,
                line_start,
                // line_start +
                (gfx.tile_size*0.5f)*line_end,
                gfx.tile_wall_color,
                4 /*thicc*/ );

        return;
    }

    fill_quad(app.sdl_renderer, player_pos, gfx.tile_size, gfx.tile_player_color);

    // @Temp: Draw the line to show which way the player is facing...
    auto line_end = Vec2i(0, 0);
    switch(player.facing) {
        case Direction::NORTH:   line_end = Vec2i( 0, -1);  break;
        case Direction::SOUTH:   line_end = Vec2i( 0,  1); break;
        case Direction::EAST:    line_end = Vec2i( 1,  0);  break;
        case Direction::WEST:    line_end = Vec2i(-1,  0); break;

        default: UNREACHABLE();
    }

    auto line_start = player_pos + (gfx.tile_size*0.5f);

    draw_line(app.sdl_renderer,
              line_start,
              // line_start +
              (gfx.tile_size*0.5f)*line_end,
              gfx.tile_wall_color,
              4 /*thicc*/ );
}

Game make_game()
{
    Game game { };
    // game.current_level.dimensions = Vec2i(20, 20);
    // game.current_level.name = "Level_1"_sv;

    load_level(game.current_level, "sandbox.level_data");

    auto &grid = game.current_level;

    // @CLEANUP: Resizing the game window to fit just the grid for now.
    auto win_size = grid.dimensions * app.visuals.tile_size;
    WINDOW_WIDTH  = max(win_size.x + WINDOW_PADDING, 1920/2);
    WINDOW_HEIGHT = max(win_size.y + WINDOW_PADDING, 1080/2);

    auto grid_size = game.current_level.dimensions * app.visuals.tile_size;
    game.current_level.screen_pos.x = my_floor(WINDOW_WIDTH*0.5f - grid_size.x*0.5f);// - app.visuals.tile_size.x()*0.5f);
    game.current_level.screen_pos.y = my_floor(WINDOW_HEIGHT*0.5f - grid_size.y*0.5f);// - app.visuals.tile_size.y()*0.5f);

    return game;
}

void update(Player &player)
{
    auto &anim = player.animation;

    if (anim.is_jumping)
    {
        if (anim.jump_t < 1.0f)
            anim.jump_t = min(anim.jump_t+app.dt*1.0f/anim.jump_time, 1.0f);
            // anim.jump_t = min(anim.jump_t+app.dt*.00001f/anim.jump_time, 1.0f);
        else {
            anim.is_jumping = false;
            anim.jump_t = 0.0f;
        }
    }
}

void on_variable_file_change(char *data, size_t size)
{
    printf("!!!!!!!! The file has changed.\n");
    printf("         Is is not %ld bytes long. Here is the file:\n\n%s\n", size, data);
}

int main()
{
    // Init logger
    Game game = make_game();

    init_renderer();
    defer(SDL_Quit());

    auto now = SDL_GetPerformanceCounter();
    auto last = 0;
    app.dt = 0.0f;

    while (!app.quit) {
        // Update dt.
        last = now;
        now = SDL_GetPerformanceCounter();
        app.dt = (float)((now - last) * 1000.0f / (float)SDL_GetPerformanceCounter());

        handle_input(game);

        // Update states.
        if(debug_panel.is_open) {
            update_debug_gui(); // Gui state
        }

        update(game.current_level.player);

        { // Render
            // @Todo: renderer_begin_scene
            clear_screen({0.2, 0.2, 0.3});

            begin_scene(app.renderer, app.renderer.camera);

            for (auto &renderable : app.renderer.renderables) {
                renderer_submit(app.renderer, renderable.shader, renderable.vao);
            }

            end_scene(app.renderer);

            SDL_GL_SwapWindow(app.window);



















            // migui_new_frame(debug_panel.ui);

            // SDL_SetRenderDrawColor(app.renderer, hex(app.visuals.bg_color));
            // SDL_RenderClear(app.renderer);

            // draw_game(game);

            // auto& ui = debug_panel.ui;
            // if (debug_panel.is_open) {
            //     ///
            //     /// Draw debug gui.
            //     ///
            //     const int gui_padding = 16; // @Hoist
            //     migui_begin(ui, debug_panel.screen_pos, gui_padding);
            //     {
            //         migui_begin_layout(ui, Migui::Layout::Kind::HORIZONTAL, gui_padding);
            //         {
            //             migui_begin_layout(ui, Migui::Layout::Kind::VERTICAL, gui_padding);
            //             {
            //                 for (size_t tool_type=0; tool_type < (size_t)Tile_Tool::Type::COUNT; ++tool_type) {
            //                     const auto current_tool = (Tile_Tool::Type)tool_type;

            //                     auto button_color = debug_panel.ui.color_palette.button_bg;
            //                     if (debug_panel.active_tool.type == current_tool)  button_color = debug_panel.ui.color_palette.button_active;

            //                     if(migui_button(debug_panel.ui, human_name(current_tool), button_color)) {
            //                         debug_panel.active_tool.type = current_tool;
            //                     }
            //                 }

            //                 if (debug_panel.active_tool.type == Tile_Tool::Type::PLACE) {
            //                     migui_begin_layout(ui, Migui::Layout::Kind::HORIZONTAL, gui_padding);
            //                     {
            //                         for (size_t i=0; i < (size_t)Grid_Tile::Kind::COUNT; ++i) {
            //                             const auto current_tile_type = (Grid_Tile::Kind)i;

            //                             auto button_color = debug_panel.ui.color_palette.button_bg;
            //                             if (debug_panel.tile_being_placed == current_tile_type)
            //                                 button_color = debug_panel.ui.color_palette.button_active;


            //                             if(migui_button(debug_panel.ui, human(current_tile_type), button_color)) {
            //                                 debug_panel.tile_being_placed = current_tile_type;
            //                             }
            //                         }
            //                     }
            //                     migui_end_layout(ui);
            //                 }

            //             }
            //             migui_end_layout(ui);

            //         }
            //         migui_end_layout(ui);

            //         // @Todo: gui background
            //         // fill_quad(r, screen_pos, screen_size, rgba(ui.color_palette.bg));

            //         // @Todo: gui border
            //         // auto &border_size = debug_panel.ui.border_size;

            //         // draw_line(r, screen_pos, screen_pos + Vec2i(0, screen_size.y()), palette_fg, border_size);
            //         // draw_line(r, screen_pos, screen_pos + Vec2i(screen_size.x(), 0), palette_fg, border_size);
            //         // draw_line(r, screen_pos + Vec2i(0, screen_size.y()), screen_pos + Vec2i(screen_size.x(), 0), palette_fg, border_size);
            //         // draw_line(r, screen_pos + Vec2i(screen_size.x(), 0), screen_pos + Vec2i(0, screen_size.y()), palette_fg, border_size);

            //     }
            //     migui_end(ui);
            // } // Draw debug gui

            SDL_RenderPresent(app.sdl_renderer);
        }
    }

    ///
    /// Shutdown, cleanup, all that shit
    ///

    // save_level(game.current_level);

    ///
    /// Finished
    ///

    printf("Application finished.\n");
    return 0;
}
