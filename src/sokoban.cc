
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include "macros.h"
#include "containers.h"

#include "hotloader.cc"
#include "renderer.cc"

#include "main.cc"
