#ifndef CONTAINERS_H_
#define CONTAINERS_H_

#include <initializer_list>
#include <iostream>

#include <stdint.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "macros.h"

using u8  = uint8_t;
using u16 = uint16_t;
using u32 = uint32_t;
using u64 = uint64_t;

template <typename T>
struct Array
{
    constexpr static size_t INITIAL_CAPACITY = 64;
    size_t capacity { 0 };
    size_t count { 0 };
    T *items { nullptr };
    Array() { }

    Array(T* data, size_t n) {
        resize(n);
        count = n;

        for (size_t i = 0; i < n; ++i) {
            items[i] = data[i];
        }
    }

    Array(std::initializer_list<T> elems)
    {
        auto it = elems.begin();
        for (size_t i = 0; i<elems.size(); ++it, ++i)
            this->push(*it);
    }

    T& push(T elem) {
        if (this->capacity == 0) {
            // if(this->items) free(this->items);
            this->count = 0;
            this->capacity = Array::INITIAL_CAPACITY;
            this->resize(this->capacity);
        }
        else if (this->count >= this->capacity) {
            this->grow();
        }

        this->items[this->count] = elem;
        return this->items[this->count++];
    }

    T* last() {
        if (this->count == 0) return nullptr;
        return &this->items[this->count];
    }

    T remove(size_t index) {
        ASSERT(index < this->count);
        auto item_being_removed = this->items[index];

        for (size_t i=index; i < this->count-1; i++) {
            this->items[i] = this->items[i+1];
        }

        return item_being_removed;
    }

    T& at(size_t index) {
        if (this->count < index) {
            for (size_t i=this->count; i <= index; ++i) {
                this->items[i] = { };
            }

            this->count = index;
        }

        return this->items[index];
    }

    T& operator[](size_t index) {
        ASSERT(index < this->count);

        return this->items[index];
    }

    T& operator[] (size_t index) const {
        ASSERT(index < this->count);

        return this->items[index];
    }

    T* begin() { return &this->items[0]; }
    T* end() { return &this->items[this->count]; }

    void resize(size_t new_size) {
        if (new_size < this->capacity)    return; // I don't want to make arrays smaller, yet. // I don't want to make arrays smaller, yet.
        this->capacity = new_size;
        this->items = (T *)realloc(this->items, new_size * sizeof(T));
        ASSERT(this->items);
    }

private:
    void grow() {
        this->capacity = 2 * this->capacity;
        this->resize(this->capacity);
    }
};

template <typename T>
struct Stack
{
    constexpr static size_t INITIAL_CAPACITY = 64;
    size_t capacity { 0 };
    size_t count { 0 };
    T *items { nullptr };

    Stack() { }

    T& push(T elem) {
        if (this->capacity == 0) {
            // if(this->items) free(this->items);
            this->count = 0;
            this->capacity = Stack::INITIAL_CAPACITY;
            this->resize(this->capacity);
        }
        else if (this->count >= this->capacity) {
            this->grow();
        }

        this->items[this->count] = elem;
        return this->items[this->count++];
    }

    T* pop() {
        if (this->count <= 0) return nullptr;

        return &this->items[this->count--];
    }

    T* peek_ptr(size_t n = 0) {
        if (this->count - n > 0) return nullptr;

        return &this->items[this->count-n-1];
    }

    T& peek(size_t n = 0) {
        ASSERT(this->count - n > 0);

        return this->items[this->count-n-1];
    }
private:
    void grow() {
        this->capacity = 2 * this->capacity;
        this->resize(this->capacity);
    }

    void resize(size_t new_size) {
        this->items = (T *)realloc(this->items, new_size * sizeof(T));
        ASSERT(this->items);
    }
};

struct String_View {
    const char* data { nullptr };
    size_t count { 0 };

    String_View() = default;
    String_View(const char* d, size_t c) : data(d), count(c) { }
    String_View(const char* d) : data(d), count(strlen(d)) { }
};

const char* to_c_str(String_View const& sv)
{
    ASSERT(sv.count > 0);

    auto buffer = (char *) malloc(sv.count + 1); // @Leak
    memcpy(buffer, sv.data, (int)sv.count);
    buffer[sv.count] = '\0';

    return buffer;
}

uint64_t to_u64(const String_View& sv)
{
    const char* c_str = to_c_str(sv);
    return atoi(c_str);
}

std::ostream& operator<<(std::ostream& out, const String_View& v) {
    out << "[SV count: " << v.count << "] `" << to_c_str(v.data) << "`";
    return out;
}

String_View operator""_sv(const char* text, size_t len) {
    return String_View(text, len);
}

bool operator==(const String_View a, const String_View b)
{
    if (a.count != b.count)  return false;

    return (strncmp(a.data, b.data, a.count) == 0);
}

bool operator!=(const String_View a, const String_View b)
{
    return !(a == b);
}

bool is_new_line(char c)
{
    return c == '\r' || c == '\n' || c == '\x00';
}
bool is_space(char c)
{
    return isspace(c) || is_new_line(c);
}

String_View trim_left(const String_View &sv) {
    size_t i = 0;
    while (i < sv.count && is_space(sv.data[i])) {
        i += 1;
    }

    return String_View(sv.data + i, sv.count - i);
}

String_View eat_line(String_View &sv) {
    // Ignore empty lines
    size_t i = 0;
    while (i < sv.count && is_new_line(sv.data[i])) {
        sv.data += 1;
        i++;
    }

    auto result = String_View(sv.data, 0);

    while (i < sv.count && !is_new_line(sv.data[i])) {
        i       += 1;
    }

    sv.data  += i+1;
    sv.count -= i+1;

    result.count = i;
    return result;
}

String_View eat_word(String_View &sv) {
    size_t i = 0;
    auto result = String_View(sv.data, 0);

    while (i < sv.count && !is_space(sv.data[i])) {
        i       += 1;
    }

    sv.data  += i+1;
    sv.count -= i+1;

    result.count = i;
    return result;
}

struct File
{
    enum class Mode
    {
        READING = 0,
        WRITING,
    };

    String_View filepath { };
    FILE* fd { nullptr };

    char *contents { nullptr };
    size_t contents_size { 0 };

    Mode mode;
};

File open(const char* filepath, File::Mode m= File::Mode::READING)
{
    File file { };
    file.filepath = String_View(filepath);
    file.mode = m;

    const char* mode;
    switch(file.mode) {
    case File::Mode::READING: mode = "r"; break;
    case File::Mode::WRITING: mode = "w"; break;
    default: UNREACHABLE();
    }

    file.fd = fopen(filepath, mode);
    if (!file.fd) {
        return file;
    }

    auto f = file.fd;

    fseek (f, 0, SEEK_END);
    auto length = ftell(f);
    fseek (f, 0, SEEK_SET);

    auto buffer = (char *)malloc (length);// @Leak
    if (buffer)  fread (buffer, 1, length, f);

    file.contents = buffer;
    file.contents_size = strlen(buffer);

    // printf("\n--------\n");
    // printf("%.*s", (int)file.contents_size, file.contents);
    // printf("\n--------\n");
    return file;
}

void clear(File &file)
{
    const char* mode = "w";

    file.mode = File::Mode::WRITING;

    if (file.fd)  file.fd = freopen(NULL, mode, file.fd);
    else          file.fd = fopen(file.filepath.data, mode);
}

void append(File &file, String_View text)
{
    ASSERT(file.fd != nullptr);

    fprintf(file.fd, "%.*s", (int)text.count, text.data);
}

void append(File &file, const char* text)
{
    ASSERT(file.fd != nullptr);

    fprintf(file.fd, "%s", text);
}

void append(File &file, u64 x)
{
    ASSERT(file.fd != nullptr);

    fprintf(file.fd, "%ld", x);
}

void append(File &file, int x)
{
    ASSERT(file.fd != nullptr);

    fprintf(file.fd, "%d", x);
}

void append(File &file, double x)
{
    ASSERT(file.fd != nullptr);

    fprintf(file.fd, "%2.6f", x);
}

template <typename T>
struct Maybe {
    bool has_value { false };
    T value; // @Speed: Do I need to copy?

    Maybe() : has_value(false), value((T)0) { }
    Maybe(bool b, T val) : has_value(b), value(val) { }
    Maybe(const Maybe &other) { *this = other; }

    bool operator ==(const Maybe& other) {
        return (other.has_value == this->has_value && other.value == this->value);
    }

    Maybe& operator =(const Maybe& other) {
        this->has_value = other.has_value;
        this->value = other.value;

        return *this;
    }
};

template <typename T>
Maybe<T> nothing() {
    Maybe<T> result {};
    result.has_value = false;

    return result;
}

template <typename T>
Maybe<T> just(T x) {
    Maybe<T> result {};
    result.has_value = true;
    result.value = x;

    return result;
}

template <typename Key, typename Value>
struct Hash_Node
{
    Key key;
    Value value;

    bool occupied;
    bool deleted;
};

template <typename Key, typename Value>
struct Hash_Map
{
    static constexpr size_t INITIAL_CAPACITY = 64;

    Hash_Node<Key, Value> *nodes;
    size_t count;
    size_t capacity;

    void put(Key const&, Value const&);
    Hash_Node<Key, Value>* get(Key const&);
    void remove(Key const&);
    void grow();
    void dump(FILE *);


private:
    ssize_t find_slot(Key const& key);
    void resize(size_t);
};

inline uint64_t hash(String_View sv, size_t capacity)
{
    return ((uint64_t)sv.data + sv.count) % capacity;
}

template <typename Key, typename Value>
void Hash_Map<Key, Value>::resize(size_t new_size)
{
    if (!this->nodes) {
        ASSERT(this->count == 0);
        ASSERT(this->capacity == 0);

        this->nodes = (Hash_Node<Key, Value>*)calloc(new_size, sizeof(Hash_Node<Key, Value>));
        this->capacity = new_size;
        return;
    }

    ASSERT (this->capacity < new_size); // Should we be allowed to shrink the table?

    nodes = (Hash_Node<Key, Value>*) realloc(nodes, new_size* sizeof(Hash_Node<Key, Value>));
    this->capacity = new_size;
}

template <typename Key, typename Value>
void Hash_Map<Key, Value>::grow()
{
    if (!this->nodes) {
        resize(INITIAL_CAPACITY);
        return;
    }

    this->resize(this->capacity * 2);
}

template <typename Key, typename Value>
void Hash_Map<Key, Value>::put(Key const& key, Value const& value)
{
    if (!this->nodes || (this->count == this->capacity)) {
        this->grow();
    }

    ASSERT(this->capacity % 2 == 0);
    uint64_t index = hash(key) & (capacity - 1);

    while (this->nodes[index].occupied) {
        auto node = this->nodes[index];
        if (node.key == key) break;

        index = (index + 1) & (capacity - 1);
    }

    this->nodes[index].key = key;
    this->nodes[index].value = value;
    this->nodes[index].occupied = true;
    ++this->count;
}

template <typename Key, typename Value>
void Hash_Map<Key, Value>::remove(Key const& key)
{
    auto hit_index = hash(key);
    auto slot = this->find_slot(key);
    if (slot < 0)  return; //! Todo: probably should feed back to caller that the node wasn't found
    if (!this->nodes[slot].occupied)  return; //! Todo: probably should feed back to caller

    auto next = (size_t)slot;
    while (hash(this->nodes[next].key) == hit_index) {
        auto &node = this->nodes[slot];
        node = {.occupied = false};

map_remove_cycle:
        next = (next+1) & (this->capacity-1);
        auto const& next_node = this->nodes[next];
        if (!next_node.occupied)  break;

        auto next_hash = hash(next_node.key) & (capacity-1);
        if ((size_t)slot <= next) {
            if ((size_t)slot < next_hash && next_hash <= next)  goto map_remove_cycle;
        }
        else {
            if ((size_t)slot < next_hash || next_hash <= next)  goto map_remove_cycle;
        }

        this->nodes[slot] = this->nodes[next];
        slot = next;
    }
}

template <typename Key, typename Value>
Hash_Node<Key, Value>* Hash_Map<Key, Value>::get(Key const& key)
{
    auto slot = this->find_slot(key);
    if (slot < 0)
        return nullptr;

    return &this->nodes[slot];
}

template <typename Key, typename Value>
ssize_t Hash_Map<Key, Value>::find_slot(Key const& key)
{
    for (size_t i = 0; i < this->capacity; ++i) {
        if (this->nodes[i].key == key) {
            return i;
        }
    }

    return -1;
}

template <typename Key, typename Value>
void Hash_Map<Key, Value>::dump(FILE *stream) {
    fprintf(stream, "Hash_Map:\n");
    for (size_t i = 0; i < this->capacity; ++i) {
        auto node = this->nodes[i];
        if (node.occupied) {
            fprintf(stream, "    map[");
            m_print(stream, node.key);
            fprintf(stream, "] = ");
            m_print(stream, node.value);
            fprintf(stream, "\n");
        }
    }
}

template <typename Key, typename Value>
struct Bucket_Hash_Map
{
    static constexpr size_t INITIAL_CAPACITY = 64;

    struct Bucket {
        Key key { };
        Array<Value> values { };
    };

    Array<Bucket> buckets { };
    size_t count;

    void put(const Key&, const Value&);

    Bucket get_bucket(const Key& key)
    {
        if (this->buckets.capacity == 0)  this->buckets.resize(INITIAL_CAPACITY);

        auto index = hash(key, this->buckets.capacity);
        return buckets.at(index);
        // for (size_t i = 0; i < this->buckets.count; ++i) {
        //     auto bucket = this->buckets[i];
        //     if (bucket.key == key){
        //         return just(bucket);
        //     }
        // }

        // return nothing<Bucket_Hash_Map::Bucket>();
    }

private:
    ssize_t find_slot(Key const& key);
};

template <typename Key, typename Value>
void Bucket_Hash_Map<Key, Value>::put(const Key& key, const Value& value)
{
    if (this->buckets.capacity == 0)  this->buckets.resize(INITIAL_CAPACITY);

    auto index = hash(key, this->buckets.capacity);
    auto bucket = this->buckets.at(index);
    bucket.key = key;
    bucket.values.push(value);
    this->count++;

    // for (size_t i = 0; i < this->buckets.count; ++i) {
    //     auto bucket = this->buckets[i];
    //     if (bucket.key == key){
    //         bucket.values.push(value);
    //         return;
    //     }
    // }
}

#endif // CONTAINERS_H_
