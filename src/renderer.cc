#include "macros.h"
#include "renderer.h"

#include "glm/glm/glm.hpp"
#include "glm/glm/gtc/matrix_transform.hpp"
#include "glm/glm/gtc/type_ptr.hpp"

float Scale = 0.0f;

void init(Renderable& renderable, Shader& shader, Array<Vertex> vertices, Array<Vertex_Index> indices)
{
    init(renderable.vao);

    init(renderable.vbo, vertices.items, vertices.count);

    Vertex_Buffer_Layout vertices_layout = {
        { Shader_Data_Type::FLOAT3, "vertex_pos"_sv },
        { Shader_Data_Type::FLOAT3, "vertex_col"_sv },
    };

    set_layout(renderable.vbo, vertices_layout);

    add_vertex_buffer(renderable.vao, renderable.vbo);

    init(renderable.ibo, indices.items, indices.count);

    set_index_buffer(renderable.vao, renderable.ibo);

    renderable.shader = shader;
}

bool init_gl(Renderer &renderer, size_t window_width, size_t window_height) {
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
    SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 2);

    GLenum glew_error = glewInit();
    if (GLEW_OK != glew_error) {
        spdlog::error("Error initialising GLEW"); // {%s}", glewGetErrorString(glew_error));
        return false;
    }

    glEnable(GL_MULTISAMPLE);

    init(renderer.camera, 0.0f, window_width, window_height, 0.0f);

    const GLchar* vertex_source = R"glsl(
        #version 330 core

        in vec3 vertex_pos;
        in vec3 vertex_col;

        uniform mat4 u_view_projection;

        out vec3 v_col;

        void main() {
            gl_Position = u_view_projection * vec4( vertex_pos, 1 );
            v_col = vertex_col;
        }
    )glsl";

    const GLchar* fragment_source = R"glsl(
        #version 330 core

        in vec3 v_col;

        out vec4 frag_color;

        void main() {
            frag_color = vec4( v_col, 1.0 );
        }
    )glsl";

    init_shader(renderer.quad_shader, vertex_source, fragment_source);

    { // Quads
        Array<Vertex> vertices { };
        vertices.push(Vertex{{ 100.0f, 200.0f, 0.0f }, { 0.3f, 0.8f, 0.2f }});
        vertices.push(Vertex{{  300.0f, 200.0f,  0.0f }, { 0.3f, 0.8f, 0.2f }});
        vertices.push(Vertex{{  300.0f,  400.0f,  0.0f }, { 0.3f, 0.8f, 0.2f }});
        vertices.push(Vertex{{ 100.0f,  400.0f,  0.0f }, { 0.3f, 0.8f, 0.2f }});

        Array<Vertex_Index> indices { };
        indices.push(0);
        indices.push(1);
        indices.push(2);
        indices.push(2);
        indices.push(3);
        indices.push(0);

        auto& quad = renderer.renderables.push({ });
        init(quad, renderer.quad_shader, vertices, indices);

        Array<Vertex> vertices2 { };
        vertices2.push(Vertex{{ 250.0f, 100.0f, 0.0f }, { 0.3f, 0.8f, 0.2f }});
        vertices2.push(Vertex{{  500.0f, 100.0f,  0.0f }, { 0.3f, 0.8f, 0.2f }});
        vertices2.push(Vertex{{  500.0f,  250.0f,  0.0f }, { 0.3f, 0.8f, 0.2f }});
        vertices2.push(Vertex{{ 250.0f,  250.0f,  0.0f }, { 0.3f, 0.8f, 0.2f }});

        Array<Vertex_Index> indices2 { };
        indices2.push(0);
        indices2.push(1);
        indices2.push(2);
        indices2.push(2);
        indices2.push(3);
        indices2.push(0);

        auto& quad2 = renderer.renderables.push({ });
        init(quad2, renderer.quad_shader, vertices2, indices2);
    }

    { // Cube
        Array<Vertex> vertices {};
        vertices.push({ { -.5f, -.5f,  .5f, 1 }, { 0, 0, 1, 1 } });
        vertices.push({ { -.5f,  .5f,  .5f, 1 }, { 1, 0, 0, 1 } });
        vertices.push({ {  .5f,  .5f,  .5f, 1 }, { 0, 1, 0, 1 } });
        vertices.push({ {  .5f, -.5f,  .5f, 1 }, { 1, 1, 0, 1 } });
        vertices.push({ { -.5f, -.5f, -.5f, 1 }, { 1, 1, 1, 1 } });
        vertices.push({ { -.5f,  .5f, -.5f, 1 }, { 1, 0, 0, 1 } });
        vertices.push({ {  .5f,  .5f, -.5f, 1 }, { 1, 0, 1, 1 } });
        vertices.push({ {  .5f, -.5f, -.5f, 1 }, { 0, 0, 1, 1 } });
        Array<Vertex_Index> indices{
            0,2,1,  0,3,2,
            4,3,0,  4,7,3,
            4,1,5,  4,0,1,
            3,6,2,  3,7,6,
            1,6,5,  1,2,6,
            7,5,6,  7,4,5
        };
    }

    return true;
}

void init(Vertex_Buffer &buffer, Vertex* data, size_t count)
{
    buffer.data = Array<Vertex>(data, count);

    glGenBuffers(1, &buffer.id);
    glBindBuffer(GL_ARRAY_BUFFER, buffer.id);

    auto size_of_one_vertex = sizeof(Vertex);
    glBufferData(GL_ARRAY_BUFFER, count*size_of_one_vertex, buffer.data.items, GL_STATIC_DRAW);
}

void bind(const Vertex_Buffer &vb)
{
    glBindBuffer(GL_ARRAY_BUFFER, vb.id);
}

void unbind(const Vertex_Buffer &vb)
{
    UNUSED(vb);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}

Vertex_Buffer_Layout::Vertex_Buffer_Layout(std::initializer_list<Vertex_Buffer_Layout_Element> elems)
    : elements(elems)
{
    update_offsets_and_stride(*this);
}

void set_layout(Vertex_Buffer &vb, Vertex_Buffer_Layout& layout)
{
    vb.layout = layout;
}

void init(Index_Buffer &buffer, Vertex_Index* data, size_t count)
{
    buffer.data = Array<Vertex_Index>(data, count);

    glGenBuffers(1, &buffer.id);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffer.id);

    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count*sizeof(Vertex_Index), buffer.data.items, GL_STATIC_DRAW);
}

void bind(const Index_Buffer &vb)
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vb.id);
}

void unbind(const Index_Buffer &vb)
{
    UNUSED(vb);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void update_offsets_and_stride(Vertex_Buffer_Layout &layout)
{
    uint32_t offset = 0;
    layout.stride = 0;

    for(size_t i = 0; i < layout.elements.count; ++i) {
        auto& element = layout.elements[i];

        element.offset = offset;

        offset += element.size;

        layout.stride += element.size;
    }
}

void bind(Vertex_Array &vao)
{
    glBindVertexArray(vao.id);
}

void unbind(Vertex_Array &)
{
    glBindVertexArray(0);
}

void add_vertex_buffer(Vertex_Array &vao, Vertex_Buffer &vbo)
{
    glBindVertexArray(vao.id);
    bind(vbo);

    uint32_t index = 0;
    for (auto &elem : vbo.layout.elements) {
        glEnableVertexAttribArray(index);
        glVertexAttribPointer(index,
                              component_count_of(elem.type),
                              gl_type(elem.type),
                              elem.normalized ? GL_TRUE : GL_FALSE,
                              vbo.layout.stride,
                              (const void*)elem.offset);

        index++;
    }


    vao.vertex_buffers.push(&vbo);
}

void set_index_buffer(Vertex_Array &vao, Index_Buffer &ibo)
{
    glBindVertexArray(vao.id);
    bind(ibo);

    vao.index_buffer = &ibo;
}

bool init_shader(Shader &shader, const char* vertex_source, const char* fragment_source)
{
    shader.id = glCreateProgram();

    GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex_shader, 1, &vertex_source, NULL);
    glCompileShader(vertex_shader);

    auto shader_did_compile = GL_FALSE;
    glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &shader_did_compile);
    if (GL_FALSE == shader_did_compile) {
        spdlog::error("Couldn't compile the temporary vertex shader.");
        // spdlog::error("Shader source was:\n{}", vertex_source[0]);
        return false;
    }

    glAttachShader(shader.id, vertex_shader);

    GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment_shader, 1, &fragment_source, NULL);
    glCompileShader(fragment_shader);

    shader_did_compile = GL_FALSE;
    glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &shader_did_compile);
    if (GL_FALSE == shader_did_compile) {
        spdlog::error("Couldn't compile the temporary fragment shader.");
        // spdlog::error("Shader source was:\n{}", fragment_source[0]);
        return false;
    }

    glAttachShader(shader.id, fragment_shader);

    glLinkProgram(shader.id);

    GLint program_did_link = GL_TRUE;
    glGetProgramiv(shader.id, GL_LINK_STATUS, &program_did_link);
    if (GL_FALSE == program_did_link) {
        spdlog::error("Couldn't link shader program..");
        return false;
    }

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    shader.vertex_source = String_View(vertex_source);
    shader.fragment_source = String_View(fragment_source);

    return true;
}

void use_shader(const Shader& shader)
{
    glUseProgram(shader.id);
}

void set_uniform(const Shader& shader, const char* name, bool value) {
    glUniform1i(glGetUniformLocation(shader.id, name), (int)value);
}

void set_uniform(const Shader& shader, const char* name, int value) {
    glUniform1i(glGetUniformLocation(shader.id, name), value);
}

void set_uniform(const Shader& shader, const char* name, float value) {
    glUniform1f(glGetUniformLocation(shader.id, name), value);
}

void set_uniform(const Shader& shader, const char* name, const glm::mat4& mat) {
    glUniformMatrix4fv(glGetUniformLocation(shader.id, name), 1, GL_FALSE, glm::value_ptr(mat));
}

static void update_view_matrix(Ortho_Camera &cam)
{
    glm::mat4 xform = glm::translate(glm::mat4(1.0f), cam.pos) *
                     glm::rotate(glm::mat4(1.0f), glm::radians(cam.z_rot), glm::vec3(0, 0, 1));

    cam.view_matrix = glm::inverse(xform);
    cam.view_projection_matrix = cam.projection_matrix * cam.view_matrix;
}

void init(Ortho_Camera& cam, float left, float right, float bot, float top)
{
    cam.projection_matrix = glm::ortho(left, right, bot, top, -1.0f, 1.0f);
    cam.view_matrix = glm::mat4(1.0f);
    cam.view_projection_matrix = cam.projection_matrix * cam.view_matrix;

}

void move(Ortho_Camera &cam, glm::vec3 pos)
{
    cam.pos = pos;
    update_view_matrix(cam);
}

void rotate(Ortho_Camera &cam, float rot)
{
    cam.z_rot = rot;
    update_view_matrix(cam);
}

void begin_scene(Renderer &renderer, Ortho_Camera &cam)
{
    renderer.state.view_projection_matrix = cam.view_projection_matrix;
}

void end_scene(Renderer &renderer)
{
    UNUSED(renderer);

    glUseProgram( 0 );
}
