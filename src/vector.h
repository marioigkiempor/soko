#ifndef VECTOR_H_
#define VECTOR_H_

#include <math.h> // @Todo: remove, using fmodf

#include "spdlog/fmt/ostr.h"
#include "./math.h"

///
/// Vec2
///

template <typename T>
struct Vec2
{
    union {
        T xs[2];
        struct {
            T x, y;
        };
    };

    Vec2() { xs[0]=0; xs[1]=0; }
    Vec2(T x0) { xs[0]=x0; xs[1]=x0; }
    Vec2(T x0, T x1) { xs[0]=x0; xs[1]=x1; }

    operator Vec2<int> ()
    {
        Vec2<int> result;
        for (size_t i=0; i<2; ++i) result.xs[i] = (int)xs[i];
        return result;
    }

    template<typename OStream>
    friend OStream &operator<<(OStream &os, const Vec2<T> &c)
    {
        return os << "[ " << c.xs[0] << "," << c.xs[1] << " ]";
    }
};

template <typename T>
inline uint64_t hash(Vec2<T> vec, size_t capacity)
{
    uint64_t result = 0;
    for (size_t i = 0; i < 2; ++i) result += vec.xs[i] * 19;
    return result % capacity;
}

template <typename T>
Vec2<T> lerp(Vec2<T> from, Vec2<T> to, float t)
{
    return from + (to - from) * t;
}

template <typename T>
Vec2<T> operator+(const Vec2<T>& a, const Vec2<T>& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]+b.xs[i];
    return result;
}

Vec2<float> operator+(const Vec2<float>& a, const Vec2<int>& b) {
    Vec2<float> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]+(float)b.xs[i];
    return result;
}

template <typename T>
Vec2<T> operator-(const Vec2<T>& a, const Vec2<T>& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]-b.xs[i];
    return result;
}

template <typename T>
Vec2<T> operator*(const Vec2<T>& a, const Vec2<T>& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]*b.xs[i];
    return result;
}

Vec2<float> operator*(const Vec2<float>& a, const Vec2<int>& b) {
    Vec2<float> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]*(float)b.xs[i];
    return result;
}

template <typename T>
Vec2<T> operator/(const Vec2<T>& a, const Vec2<T>& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]/b.xs[i];
    return result;
}

template <typename T, typename S>
Vec2<T> operator+(const S& b, const Vec2<T>& a) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]+b;
    return result;
}

template <typename T, typename S>
Vec2<T> operator-(const S& b, const Vec2<T>& a) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]-b;
    return result;
}

template <typename T, typename S>
Vec2<T> operator*(const S& b, const Vec2<T>& a) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]*b;
    return result;
}

template <typename T, typename S>
Vec2<T> operator/(const S& b, const Vec2<T>& a) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]/b;
    return result;
}

template <typename T, typename S>
Vec2<T> operator+(const Vec2<T>& a, const S& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]+b;
    return result;
}

template <typename T, typename S>
Vec2<T> operator-(const Vec2<T>& a, const S& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]-b;
    return result;
}

template <typename T, typename S>
Vec2<T> operator*(const Vec2<T>& a, const S& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]*b;
    return result;
}

template <typename T, typename S>
Vec2<T> operator/(const Vec2<T>& a, const S& b) {
    Vec2<T> result;
    for (size_t i=0; i<2; ++i) result.xs[i] = a.xs[i]/b;
    return result;
}

template <typename T>
Vec2<int> floor(const Vec2<T>& a) {
    Vec2<int> result;
    for (size_t i=0; i<2; ++i)   result.xs[i] = my_floor(a.xs[i]);
    return result;
}

///
/// Vec3
///

template <typename T>
struct Vec3
{
    union {
        T xs[3];
        struct {
            T x, y, z;
        };
    };

    Vec3()                 { xs[0]=0;  xs[1]=0;  xs[2]=0; }
    Vec3(T x0)             { xs[0]=x0; xs[1]=x0; xs[2]=x0; }
    Vec3(T x0, T x1, T x2) { xs[0]=x0; xs[1]=x1; xs[2]=x2; }
};

template <typename T>
Vec3<T> operator+(const Vec3<T>& a, const Vec3<T>& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]+b.xs[i];
    return result;
}

template <typename T>
Vec3<T> operator-(const Vec3<T>& a, const Vec3<T>& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]-b.xs[i];
    return result;
}

template <typename T>
Vec3<T> operator*(const Vec3<T>& a, const Vec3<T>& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]*b.xs[i];
    return result;
}

template <typename T>
Vec3<T> operator/(const Vec3<T>& a, const Vec3<T>& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]/b.xs[i];
    return result;
}

template <typename T, typename S>
Vec3<T> operator+(const Vec3<T>& a, const S& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]+b;
    return result;
}

template <typename T, typename S>
Vec3<T> operator-(const Vec3<T>& a, const S& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]-b;
    return result;
}

template <typename T, typename S>
Vec3<T> operator*(const Vec3<T>& a, const S& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]*b;
    return result;
}

template <typename T, typename S>
Vec3<T> operator/(const Vec3<T>& a, const S& b) {
    Vec3<T> result;
    for (size_t i=0; i<3; ++i) result.xs[i] = a.xs[i]/b;
    return result;
}

///
/// Vec4
///

template <typename T>
struct Vec4
{
    union {
        T xs[4];
        struct {
            T x, y, z, w;
        };
    };

    Vec4()                       { xs[0]=0;  xs[1]=0;  xs[2]=0;  xs[3]=0; }
    Vec4(T x0)                   { xs[0]=x0; xs[1]=x0; xs[2]=x0; xs[3]=x0; }
    Vec4(T x0, T x1, T x2, T x3) { xs[0]=x0; xs[1]=x1; xs[2]=x2; xs[3]=x3; }
};

template <typename T>
Vec4<T> operator+(const Vec4<T>& a, const Vec4<T>& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]+b.xs[i];
    return result;
}

template <typename T>
Vec4<T> operator-(const Vec4<T>& a, const Vec4<T>& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]-b.xs[i];
    return result;
}

template <typename T>
Vec4<T> operator*(const Vec4<T>& a, const Vec4<T>& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]*b.xs[i];
    return result;
}

template <typename T>
Vec4<T> operator/(const Vec4<T>& a, const Vec4<T>& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]/b.xs[i];
    return result;
}

template <typename T, typename S>
Vec4<T> operator+(const Vec4<T>& a, const S& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]+b;
    return result;
}

template <typename T, typename S>
Vec4<T> operator-(const Vec4<T>& a, const S& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]-b;
    return result;
}

template <typename T, typename S>
Vec4<T> operator*(const Vec4<T>& a, const S& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]*b;
    return result;
}

template <typename T, typename S>
Vec4<T> operator/(const Vec4<T>& a, const S& b) {
    Vec4<T> result;
    for (size_t i=0; i<4; ++i) result.xs[i] = a.xs[i]/b;
    return result;
}

using Vec2i = Vec2<int>;
using Vec3i = Vec3<int>;
using Vec4i = Vec4<int>;
using Vec2f = Vec2<float>;
using Vec3f = Vec3<float>;
using Vec4f = Vec4<float>;

struct Rgba : public Vec4<float>
{
    Rgba() = default;

    Rgba(int r_, int g_, int b_, int a_) {
        r() = ((float)(r_ & 0xFF) / 255.0f);
        g() = ((float)(g_ & 0xFF) / 255.0f);
        b() = ((float)(b_ & 0xFF) / 255.0f);
        a() = ((float)(a_ & 0xFF) / 255.0f);
    }

    Rgba(unsigned int hex) {
        r() = (float)(((hex>>24) & 0xFF) / 255.0f);
        g() = (float)(((hex>>16) & 0xFF) / 255.0f);
        b() = (float)(((hex>>8) & 0xFF) / 255.0f);
        a() = (float)(((hex>>0) & 0xFF) / 255.0f);
    }

    float const &r() const { return this->x; }
    float const &g() const { return this->y; }
    float const &b() const { return this->z; }
    float const &a() const { return this->w; }

    float &r() { return this->x; }
    float &g() { return this->y; }
    float &b() { return this->z; }
    float &a() { return this->w; }

    unsigned int u32() {
        return (
            (((int)(r()*255))<<24) |
            (((int)(g()*255))<<16) |
            (((int)(b()*255))<< 8) |
            (((int)(a()*255))    )
        );
    }

};

struct Hsla : public Vec4<float>
{
    Hsla() = default;

    Hsla(float h_, float s_, float l_) {
        x = h_;
        y = s_;
        z = l_;
        w = 1.0f;
    }

    Hsla(float h_, float s_, float l_, float a_) {
        x = h_;
        y = s_;
        z = l_;
        w = a_;
    }

    Hsla(Rgba rgba) {
        x = (rgba.r() / 255.0f);
        y = (rgba.g() / 255.0f);
        z = (rgba.b() / 255.0f);
        w = (rgba.a() / 255.0f);
    }

    Hsla(unsigned int hex) {
        x = (float)((hex>>24) & 0xFF);
        y = (float)((hex>>16) & 0xFF);
        z = (float)((hex>>8) & 0xFF);
        w = (float)((hex>>0) & 0xFF);
    }

    float const &h() const { return this->xs[0]; }
    float const &s() const { return this->xs[1]; }
    float const &l() const { return this->xs[2]; }
    float const &a() const { return this->xs[3]; }

    float &h() { return this->xs[0]; }
    float &s() { return this->xs[1]; }
    float &l() { return this->xs[2]; }
    float &a() { return this->xs[3]; }

    // inline unsigned int u32() {
    //     return (((x()<<24)) | ((y()<<16)) | ((z()<<8)) | (w())); }
};

Rgba rgba(Hsla const& hsla) {
    Rgba rgba;

    rgba.r() = clamp(0.0f, abs(my_fmod(0.0f + hsla.h()*6.0f, 6.0f) - 3.0f) - 1.0f, 1.0f);
    rgba.g() = clamp(0.0f, abs(my_fmod(4.0f + hsla.h()*6.0f, 6.0f) - 3.0f) - 1.0f, 1.0f);
    rgba.b() = clamp(0.0f, abs(my_fmod(2.0f + hsla.h()*6.0f, 6.0f) - 3.0f) - 1.0f, 1.0f);
    rgba.a() = hsla.a();

    rgba.r() = hsla.l() + hsla.s() * (rgba.r() - 0.5f) * (1.0f - abs(2.0f*hsla.l() - 1.0f));
    rgba.g() = hsla.l() + hsla.s() * (rgba.g() - 0.5f) * (1.0f - abs(2.0f*hsla.l() - 1.0f));
    rgba.b() = hsla.l() + hsla.s() * (rgba.b() - 0.5f) * (1.0f - abs(2.0f*hsla.l() - 1.0f));

    return rgba;
}

struct Mat4f {
    float m[4][4];
};

// template <typename T, size_t N>
// struct Vector {
//     T xs[N] = { 0 };

//     Vector<T, N> operator+(Vector<T, N> other) const {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] + other.xs[i]; return res;}
//     Vector<T, N> operator-(Vector<T, N> other) const {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] - other.xs[i]; return res;}
//     Vector<T, N> operator*(Vector<T, N> other) const {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] * other.xs[i]; return res;}
//     Vector<T, N> operator/(Vector<T, N> other) const {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] / other.xs[i]; return res;}
//     Vector<T, N> operator+=(Vector<T, N> other) {for (size_t i=0; i < N; ++i) this->xs[i] = this->xs[i] + other.xs[i]; return *this;}
//     Vector<T, N> operator-=(Vector<T, N> other) {for (size_t i=0; i < N; ++i) this->xs[i] = this->xs[i] - other.xs[i]; return *this;}
//     Vector<T, N> operator*=(Vector<T, N> other) {for (size_t i=0; i < N; ++i) this->xs[i] = this->xs[i] * other.xs[i]; return *this;}
//     Vector<T, N> operator/=(Vector<T, N> other) {for (size_t i=0; i < N; ++i) this->xs[i] = this->xs[i] / other.xs[i]; return *this;}
//     Vector<T, N> operator+(T n) const            {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] + n;           return res;}
//     Vector<T, N> operator-(T n) const            {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] - n;           return res;}
//     Vector<T, N> operator*(T n) const            {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] * n;           return res;}
//     Vector<T, N> operator/(T n) const            {Vector<T, N> res; for (size_t i=0; i < N; ++i) res.xs[i] = this->xs[i] / n;           return res;}

//     bool        operator==(Vector<T, N> other) { for (size_t i=0; i < N; ++i) { if (this->xs[i] != other.xs[i])  return false; } return true; }
// };

// template <typename T>
// struct Vec2 : public Vector<T, 2> {
//     Vec2<T>()           { this->xs[0] = 0;  this->xs[1] = 0; }
//     Vec2<T>(T x0)       { this->xs[0] = x0; this->xs[1] = x0; }
//     Vec2<T>(T x0, T x1) { this->xs[0] = x0; this->xs[1] = x1; }

//     T& x()             { return this->xs[0]; }
//     T& y()             { return this->xs[1]; }
//     const T& x() const { return this->xs[0]; }
//     const T& y() const { return this->xs[1]; }
// };

// template<typename T>
// std::ostream& operator<<(std::ostream& out, const Vec2<T> vec)
// {
//     out << "{";
//     for (size_t i = 0; i < 2; ++i) {
//         out << vec.xs[i];
//         if (i != 2-1) out << " ";
//     }
//     out << "}";
//     return out;
// }

// template <typename T>
// struct Vec3 : public Vector<T, 3> {
//     Vec3<T>()                 { this->xs[0] = 0;  this->xs[1] = 0;  this->xs[2] = 0; }
//     Vec3<T>(T x0)             { this->xs[0] = x0; this->xs[1] = x0; this->xs[2] = x0; }
//     Vec3<T>(T x0, T x1, T x2) { this->xs[0] = x0; this->xs[1] = x1; this->xs[2] = x2; }

//     T& x() { return this->xs[0]; }
//     T& y() { return this->xs[1]; }
//     T& z() { return this->xs[2]; }
//     const T& x() const { return this->xs[0]; }
//     const T& y() const { return this->xs[1]; }
//     const T& z() const { return this->xs[2]; }
// };

// template<typename T>
// std::ostream& operator<<(std::ostream& out, const Vec3<T> vec)
// {
//     out << "{";
//     for (size_t i = 0; i < 3; ++i) {
//         out << vec.xs[i];
//         if (i != 3-1) out << " ";
//     }
//     out << "}";
//     return out;
// }

// template <typename T>
// struct Vec4 : public Vector<T, 4>{

//     Vec4<T>()                       { this->xs[0] = 0;  this->xs[1] = 0;  this->xs[2] = 0;  this->xs[3] = 0; }
//     Vec4<T>(T x0)                   { this->xs[0] = x0; this->xs[1] = x0; this->xs[2] = x0; this->xs[3] = x0; }
//     Vec4<T>(T x0, T x1, T x2, T x3) { this->xs[0] = x0; this->xs[1] = x1; this->xs[2] = x2; this->xs[3] = x3; }

//     T& x() { return this->xs[0]; }
//     T& y() { return this->xs[1]; }
//     T& z() { return this->xs[2]; }
//     T& w() { return this->xs[3]; }
//     const T& x() const { return this->xs[0]; }
//     const T& y() const { return this->xs[1]; }
//     const T& z() const { return this->xs[2]; }
//     const T& w() const { return this->xs[3]; }
// };

// template<typename T>
// std::ostream& operator<<(std::ostream& out, const Vec4<T> vec)
// {
//     out << "{";
//     for (size_t i = 0; i < 4; ++i) {
//         out << vec.xs[i];
//         if (i != 4-1) out << " ";
//     }
//     out << "}";
//     return out;
// }

// typedef Vec2<int> Vec2i;

// // struct Vec2i : public Vec2<int>
// // {
// //     Vec2i() : Vec2<int>() {};
// //     Vec2i(int x0) : Vec2<int>(x0) {};
// //     Vec2i(int x, int y) : Vec2<int>(x, y) {}
// // };
// struct Vec3i : public Vec3<int>   {
//     Vec3i() : Vec3<int>() {};
//     Vec3i(int x0) : Vec3<int>(x0) {};
//     Vec3i(int x, int y, int z) : Vec3<int>(x, y, z) {}
// };
// struct Vec4i : public Vec4<int>   {
//     Vec4i() : Vec4<int>() {};
//     Vec4i(int x0) : Vec4<int>(x0) {};
//     Vec4i(int x, int y, int z, int w) : Vec4<int>(x, y, z, w) {}
// };

// struct Vec2f : public Vec2<float> {
//     Vec2f() : Vec2<float>() {};
//     Vec2f(float x0) : Vec2<float>(x0) {};
//     Vec2f(float x, float y) : Vec2<float>(x, y) {}
// };

// struct Vec3f : public Vec3<float> {
//     Vec3f() : Vec3<float>() {};
//     Vec3f(float x0) : Vec3<float>(x0) {};
//     Vec3f(float x, float y, float z) : Vec3<float>(x, y, z) {}
// };

// struct Vec4f : public Vec4<float> {
//     Vec4f() : Vec4<float>() {};
//     Vec4f(float x0) : Vec4<float>(x0) {};
//     Vec4f(float x, float y, float z, float w) : Vec4<float>(x, y, z, w) {}
// };

// Vec3f fmod(Vec3f vec, float x)
// {
//     Vec3f result;

//     for (size_t i=0; i < 3; ++i)
//         result.xs[i] = my_fmod(vec.xs[i], x);

//     return result;
// }

// template <typename T, size_t N>
// Vector<float,N> to_vecf(Vector<T,N> const& vec)
// {
//     Vector<int, N> result;
//     for (size_t i = 0; i < N; ++i)
//         result.xs[i] = (float)vec.xs[i];
//     return result;
// }

// template <typename T, size_t N>
// Vector<int,N> to_veci(Vector<T,N> const& vec)
// {
//     Vector<int, N> result;
//     for (size_t i = 0; i < N; ++i)
//         result.xs[i] = (int)vec.xs[i];
//     return result;
// }

// template <typename T, size_t N>
// Vector<T,N> abs(Vector<T,N> const& vec)
// {
//     Vector<T,N> result;

//     for (size_t i = 0; i < N; i++) {
//         result.xs[i] = abs(vec.xs[i]);
//     }

//     return result;
// }

// template <typename T, size_t N>
// Vector<int,N> floor(Vector<T,N> const& vec)
// {
//     Vector<T,N> result;

//     for (size_t i = 0; i < N; i++) {
//         result.xs[i] = my_floor(vec.xs[i]);
//     }

//     return result;
// }

// template <typename T, size_t N>
// Vector<T, N> lerp(Vector<T, N> from, Vector<T, N> to, float t)
// {
//     return from + (to - from) * t;
// }

// template <typename T, size_t N>
// Vector<T, N> clamp(T min, Vector<T, N> x, T max)
// {
//     Vector<T, N> result;
//     for (size_t i = 0; i<N; ++i) {
//         if (x.xs[i] < min)  result.xs[i] = min;
//         if (x.xs[i] > max)  result.xs[i] = max;
//         result.xs[i] = x.xs[i];
//     }

//     return result;
// }

// template <typename T, size_t N>
// inline uint64_t hash(Vector<T, N> vec, size_t capacity)
// {
//     uint64_t result = 0;
//     for (size_t i = 0; i < N; ++i) result += vec.xs[i] * 19;

//     return result%capacity;
// }

// Hsla hsla(Rgba const& rgba) {
//     float r_float = rgba.r() / 255.0f;
//     float g_float = rgba.g() / 255.0f;
//     float b_float = rgba.b() / 255.0f;

//     float rgb_min = min(r_float, g_float, b_float);
//     float rgb_max = max(r_float, g_float, b_float);

//     float delta = rgb_max - rgb_min;

//     float h = 0;
//     float s = 0;
//     float l = ((rgb_max+rgb_min) / 2.0f);

//     constexpr float delta_tolerance = 0.001f;

//     if (delta < -delta_tolerance || delta > delta_tolerance) {
//         if (l < 0.5f) {
//             s = (delta / (rgb_max+rgb_min));
//         }
//         else {
//             s = (delta / (2.0f-delta));
//         }

//         if (r_float < (rgb_max-delta_tolerance) || r_float > (rgb_max+delta_tolerance)) {
//             h = (g_float-b_float) / delta;
//         }
//         if (g_float < (rgb_max-delta_tolerance) || g_float > (rgb_max+delta_tolerance)) {
//             h = 2.0f + (b_float-r_float) / delta;
//         }
//         if (b_float < (rgb_max-delta_tolerance) || b_float > (rgb_max+delta_tolerance)) {
//             h = 4.0f + (r_float-g_float) / delta;
//         }
//     }

//     return Hsla(h, s, l, rgba.a());
// }

// Vec3f modf(Vec3f vec, float x) {
//     Vec3f result;

//     for (size_t i = 0; i < 3; ++i) {
//         result.xs[i] = my_fmod(vec.xs[i], x);
//     }

//     return result;
// }


// template <typename T>
// struct Rect
// {
//     Vec2<T> pos;
//     Vec2<T> size;
// };


#endif // VECTOR_H_
