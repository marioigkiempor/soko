#ifndef MACROS_H_
#define MACROS_H_
#include <stdio.h>
#include <stdlib.h>
#include <execinfo.h>
#include <cxxabi.h>
#include <stdio.h>

////////////////////////////////////////
///    Helpers
////////////////////////////////////////

/** Print a demangled stack backtrace of the caller function to FILE* out. */
// Source: https://panthema.net/2008/0901-stacktrace-demangled/
static inline void print_stacktrace(FILE *out = stderr, unsigned int max_frames = 63)
{
    fprintf(out, "stack trace:\n");

    // storage array for stack trace address data
    void* addrlist[max_frames+1];

    // retrieve current stack addresses
    int addrlen = backtrace(addrlist, sizeof(addrlist) / sizeof(void*));

    if (addrlen == 0) {
    fprintf(out, "  <empty, possibly corrupt>\n");
    return;
    }

    // resolve addresses into strings containing "filename(function+address)",
    // this array must be free()-ed
    char** symbollist = backtrace_symbols(addrlist, addrlen);

    // allocate string which will be filled with the demangled function name
    size_t funcnamesize = 256;
    char* funcname = (char*)malloc(funcnamesize);

    // iterate over the returned symbol lines. skip the first, it is the
    // address of this function.
    for (int i = 1; i < addrlen; i++)
    {
    char *begin_name = 0, *begin_offset = 0, *end_offset = 0;

    // find parentheses and +address offset surrounding the mangled name:
    // ./module(function+0x15c) [0x8048a6d]
    for (char *p = symbollist[i]; *p; ++p)
    {
        if (*p == '(')
        begin_name = p;
        else if (*p == '+')
        begin_offset = p;
        else if (*p == ')' && begin_offset) {
        end_offset = p;
        break;
        }
    }

    if (begin_name && begin_offset && end_offset
        && begin_name < begin_offset)
    {
        *begin_name++ = '\0';
        *begin_offset++ = '\0';
        *end_offset = '\0';

        // mangled name is now in [begin_name, begin_offset) and caller
        // offset in [begin_offset, end_offset). now apply
        // __cxa_demangle():

        int status;
        char* ret = abi::__cxa_demangle(begin_name,
                        funcname, &funcnamesize, &status);
        if (status == 0) {
        funcname = ret; // use possibly realloc()-ed string
        fprintf(out, "  %s : %s+%s\n",
            symbollist[i], funcname, begin_offset);
        }
        else {
        // demangling failed. Output function name as a C function with
        // no arguments.
        fprintf(out, "  %s : %s()+%s\n",
            symbollist[i], begin_name, begin_offset);
        }
    }
    else
    {
        // couldn't parse the line? print the whole line.
        fprintf(out, "  %s\n", symbollist[i]);
    }
    }

    free(funcname);
    free(symbollist);
}

///////////////////////////////
#define ASSERT(condition)                                               \
    do {                                                                \
        if (!(condition)) {                                             \
            printf("%s:%d: Assertion failed!\n\t%s\n", __FILE__, __LINE__, #condition); \
            print_stacktrace();                                         \
            exit(1);                                                    \
        }                                                               \
    } while(0);

#define UNREACHABLE()                                           \
    do {                                                        \
        printf("%s:%d:\n\tUnreachable.\n", __FILE__, __LINE__);    \
        exit(1);                                                \
    } while(0);

#define HEX_COLOR(x) ((x>>24) & 0xFF), ((x>>16) & 0xFF), ((x>>8) & 0xFF), ((x>>0) & 0xFF)

#define UNUSED(x) ((void)x);

#define TODO(str, ...)                                  \
    do {                                                \
        printf("%s:%d: Todo:\n\t", __FILE__, __LINE__); \
        printf(str, ##__VA_ARGS__);                     \
        printf("\n");                                   \
        exit(1);                                        \
    } while(0);
#define UNIMPLEMENTED() TODO("%s is not implemented.", __func__)

///
/// Defer
/// https://www.reddit.com/r/ProgrammerTIL/comments/58c6dx/til_how_to_defer_in_c/

template <typename F>
struct saucy_defer {
    F f;
    saucy_defer(F f_) : f(f_) {}
    ~saucy_defer() { f(); }
};

template <typename F>
saucy_defer<F> defer_func(F f) {
    return saucy_defer<F>(f);
}

#define DEFER_1(x, y) x##y
#define DEFER_2(x, y) DEFER_1(x, y)
#define DEFER_3(x)    DEFER_2(x, __COUNTER__)
#define defer(code)   auto DEFER_3(_defer_) = defer_func([&](){code;})


#endif // MACROS_H_
