#ifndef RENDERER_H_
#define RENDERER_H_

#include <initializer_list>

#include <GL/glew.h>
#include <GL/gl.h>

#include "glm/glm/glm.hpp"

#include <SDL2/SDL.h>
#include <SDL2/SDL_opengl.h>
#include "SDL2/SDL_ttf.h"
#include "SDL2_gfxPrimitives.h"

#include "dbg.h"
#include "vector.h"
#include "containers.h"

struct Vertex
{
    glm::vec3 pos { };
    glm::vec3 col { };
};

enum class Shader_Data_Type
{
    NONE = 0,
    FLOAT,
    FLOAT2,
    FLOAT3,
    FLOAT4,
    INT,
    INT2,
    INT3,
    INT4,
    MAT2,
    MAT3,
    MAT4,
    BOOL,
};

uint32_t component_count_of(Shader_Data_Type type)
{
    switch(type)
    {
    case Shader_Data_Type::NONE:   return 0;
    case Shader_Data_Type::INT:    return 1;
    case Shader_Data_Type::INT2:   return 2;
    case Shader_Data_Type::INT3:   return 3;
    case Shader_Data_Type::INT4:   return 4;
    case Shader_Data_Type::FLOAT:  return 1;
    case Shader_Data_Type::FLOAT2: return 2;
    case Shader_Data_Type::FLOAT3: return 3;
    case Shader_Data_Type::FLOAT4: return 4;
    case Shader_Data_Type::MAT2:   return 2*2;
    case Shader_Data_Type::MAT3:   return 3*3;
    case Shader_Data_Type::MAT4:   return 4*4;
    case Shader_Data_Type::BOOL:   return 1;

    default: UNREACHABLE();
    }

    UNREACHABLE();
}

uint32_t size_of(Shader_Data_Type type)
{
    return component_count_of(type) * 4;
}

GLenum gl_type(Shader_Data_Type type)
{
    switch(type)
    {
    case Shader_Data_Type::NONE:   return GL_FLOAT;
    case Shader_Data_Type::INT:    return GL_INT;
    case Shader_Data_Type::INT2:   return GL_INT;
    case Shader_Data_Type::INT3:   return GL_INT;
    case Shader_Data_Type::INT4:   return GL_INT;
    case Shader_Data_Type::FLOAT:  return GL_FLOAT;
    case Shader_Data_Type::FLOAT2: return GL_FLOAT;
    case Shader_Data_Type::FLOAT3: return GL_FLOAT;
    case Shader_Data_Type::FLOAT4: return GL_FLOAT;
    case Shader_Data_Type::MAT2:   return GL_FLOAT;
    case Shader_Data_Type::MAT3:   return GL_FLOAT;
    case Shader_Data_Type::MAT4:   return GL_FLOAT;
    case Shader_Data_Type::BOOL:   return GL_INT;

    default: UNREACHABLE();
    }

    UNREACHABLE();
}

struct Vertex_Buffer_Layout_Element
{
    Shader_Data_Type type;
    String_View name;

    uint64_t offset;
    uint32_t size;

    bool normalized;

    Vertex_Buffer_Layout_Element(Shader_Data_Type t, String_View n, bool norm = false)
        : type(t), name(n), offset(0), size(size_of(t)), normalized(norm)
    {
    }
};

struct Vertex_Buffer_Layout
{
    Array<Vertex_Buffer_Layout_Element> elements;
    uint32_t stride;

    Vertex_Buffer_Layout(std::initializer_list<Vertex_Buffer_Layout_Element> elems);
};

void update_offsets_and_stride(Vertex_Buffer_Layout &);

struct Vertex_Buffer
{
    unsigned int id;

    Array<Vertex> data;

    Vertex_Buffer_Layout layout;
};

void init(Vertex_Buffer &, Vertex *, size_t count);

void bind(const Vertex_Buffer &vb);
void unbind(const Vertex_Buffer &vb);
void set_layout(Vertex_Buffer &vb, Vertex_Buffer_Layout&);

using Vertex_Index = unsigned int;
struct Index_Buffer
{
    unsigned int id;

    Array<Vertex_Index> data;
};

void init(Index_Buffer &, Vertex_Index*, size_t);
void unbind(const Index_Buffer &vb);
void bind(const Index_Buffer &vb);

struct Vertex_Array
{
    unsigned int id;

    Array<Vertex_Buffer*> vertex_buffers {};
    Index_Buffer *index_buffer {nullptr};
};

void init(Vertex_Array &vao)
{
    glCreateVertexArrays(1, &vao.id);
}

void bind(Vertex_Array &);
void unbind(Vertex_Array &);
void add_vertex_buffer(Vertex_Array &, Vertex_Buffer &);
void set_index_buffer(Vertex_Array &, Index_Buffer &);

struct Shader
{
    unsigned int id;

    String_View vertex_source;
    String_View fragment_source;
};

bool init_shader(Shader &shader, const char* vertex_source, const char* fragment_source);

void use_shader(const Shader& shader);
void set_uniform(const Shader& shader, const char* name, bool value);
void set_uniform(const Shader& shader, const char* name, int value);
void set_uniform(const Shader& shader, const char* name, float value);
void set_uniform(const Shader& shader, const char* name, const glm::mat4& value);

struct Ortho_Camera
{
    glm::mat4 projection_matrix;
    glm::mat4 view_matrix;
    glm::mat4 view_projection_matrix;

    glm::vec3 pos;
    float z_rot;
};

void init(Ortho_Camera &cam, float left, float right, float bot, float top);
void move(Ortho_Camera &cam, glm::vec3 pos);
void rotate(Ortho_Camera &cam, float rot);

struct Renderable
{
    Vertex_Array  vao;
    Vertex_Buffer vbo;
    Index_Buffer  ibo;

    Shader       shader;
};

struct Renderer
{
    Array<Renderable> renderables {};

    Ortho_Camera camera;

    Shader quad_shader;

    struct {
        glm::mat4 view_projection_matrix;
    } state;
};

void begin_scene(Renderer &renderer, Ortho_Camera &camera);
void end_scene(Renderer &renderer);

bool init_gl(Renderer &renderer, size_t window_width, size_t window_height);

void clear_screen(Vec3f col) {
    glClearColor(col.x, col.y, col.z, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);
}

void renderer_submit(Renderer &renderer, const Shader shader, Vertex_Array vao) {
    if (!vao.index_buffer)  return; // Incomplete: report failure

    use_shader(shader);
    set_uniform(shader, "u_view_projection", renderer.state.view_projection_matrix);

    bind(vao);

    glDrawElements(GL_TRIANGLES, vao.index_buffer->data.count, GL_UNSIGNED_INT, NULL);
}

#endif // RENDERER_H_
