
#!/usr/bin/env bash

exec="sokoban"
exec="bin/$exec"

cc="g++"
cflags="-rdynamic -lGL -lGLEW `sdl2-config --libs --cflags` -lSDL2_gfx -lSDL2_ttf -ggdb3 -std=c++17 -Wdouble-promotion -Wunused -Wall -O0 -Wno-missing-braces -Wextra -Wno-missing-field-initializers -Wformat=2 -Wswitch-default -Wswitch-enum -Wcast-align -Wpointer-arith -Wstrict-overflow=5 -Winline -Wundef -Wcast-qual -Wshadow -Wunreachable-code -Wfloat-equal -Wstrict-aliasing=2 -Wredundant-decls -Wnull-dereference -fno-omit-frame-pointer -fno-common -fstrict-aliasing"

# srcs="$(find src/ -type f -name '*.cc' | sed -z 's/\n/ /g')"
srcs="src/sokoban.cc"

includes="$(find src/ -type d | sed -z 's/\n/ /g;s/ $//;s/ / -I/g;s/^/-I/')"

set -xe
$cc $srcs -o $exec $cflags $includes
